#include "PlottingTools/PurityTools.h"

using namespace std;
using std::make_pair;

// ___________________________________________________________

vector<pair<double,double> > PurityTools::GetPurity(float x1, float x2, float y1, float y2, TH2D *h2)
{

  double purity = -1.;
  double error = 0.;

  vector<pair<double,double> > result;

  if (!h2) {
    cout << "No 2D histo to work with! Null pointer..." << endl;
    result.push_back(make_pair(purity,error));
    return result;
  }

  //  Int_t FindBin(Double_t x, Double_t y = 0, Double_t z = 0)
  //  Double_t Integral(Int_t binx1, Int_t binx2, Int_t biny1, Int_t biny2, Option_t* option = "") const
  //  Double_t IntegralAndError(Int_t binx1, Int_t binx2, Int_t biny1, Int_t biny2, Double_t& err, Option_t* option = "") const
  //
  //    y
  //    ^
  //    | A (signal)  | |  B
  // y2 +-------------+-+--------
  //    |      C      | |  D
  // y1 +-------------+-+--------
  //    |             | |           
  //    +-------------+-+--------> x
  //                 x1 x2
  //
  //
  // n.b.: signal region is defined between y1,y2
  //     : background region is expected to be from y2 to the last bin


  double Aallerr = 0.;
  double Aall = h2 -> IntegralAndError( 0, h2 -> GetXaxis() -> FindBin(x1-0.001),
                                     h2 -> GetYaxis() -> FindBin(y2), h2 -> GetYaxis() -> GetNbins(),
                                     Aallerr);

  double Berr = 0.;
  double B = h2 -> IntegralAndError( h2 -> GetXaxis() -> FindBin(x2), h2 -> GetXaxis() -> GetNbins(),
                                     h2 -> GetYaxis() -> FindBin(y2), h2 -> GetYaxis() -> GetNbins(),
                                     Berr);
  double Derr = 0.;
  double D = h2 -> IntegralAndError( h2 -> GetXaxis() -> FindBin(x2),h2 -> GetXaxis() -> GetNbins(),
                                     h2 -> GetYaxis() -> FindBin(y1),  h2 -> GetYaxis() -> FindBin(y2-0.001),
                                     Derr);
  double Cerr = 0.;
  double C    = h2 -> IntegralAndError( 0, h2 -> GetXaxis() -> FindBin(x1-0.001),
                                        h2 -> GetYaxis() -> FindBin(y1), h2 -> GetYaxis() -> FindBin(y2-0.001),
                                        Cerr);


  double Abg = 0.;
  double Abgerr = 0.;
  if (B > 0.) {
    Abg = C*B/D;
    Abgerr = sqrt( pow(Cerr*B/(D), 2) + pow(C*Berr/(D), 2) + pow(C*B*Derr/(D*D), 2) );

    purity = 1 - Abg/Aall;
    if (purity < 0) purity = 0;
    error = pow(Abg*Aallerr/(Aall*Aall), 2) + pow(Cerr*B/(D*Aall), 2) + pow(C*Berr/(D*Aall), 2) + pow(C*B*Derr/(D*D*Aall), 2);
    error = sqrt(error);

  }

  cout << " Aall: " << Aall << endl
       << " B: " << B << endl
       << " C: " << C << endl
       << " D: " << D << endl
       << " Aallerr: " << Aallerr << endl
       << " Berr: " << Berr << endl
       << " Cerr: " << Cerr << endl
       << " Derr: " << Derr << endl
       << " Abg: " << Abg << endl
       << " purity: " << purity << endl
       << " error: " << error << endl
       << endl;

  result.push_back(std::make_pair(purity,error));
  result.push_back(std::make_pair(Aall,Aallerr));
  result.push_back(std::make_pair(B,Berr));
  result.push_back(std::make_pair(C,Cerr));
  result.push_back(std::make_pair(D,Derr));
  result.push_back(std::make_pair(Abg,Abgerr));

  return result;

}

// ___________________________________________________________
