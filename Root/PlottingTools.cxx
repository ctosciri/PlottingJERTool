#include "PlottingTools/PlottingTools.h"

using namespace std;

// ___________________________________________________________

void PlottingTools::PrepareTwoPadCanvas( TCanvas* canvas,
                                         TString xTitle, TString yTitle1, TString yTitle2,
                                         float xmin, float xmax,
                                         float ymin1, float ymax1,
                                         float ymin2, float ymax2,
                                         int nDivisions)
{
    TVirtualPad* pad1 = canvas->cd(1);
    pad1->Clear();
    pad1->SetPad(0.0, 0.4, 1.0, 1.0);
    pad1->GetFrame()->SetBorderMode(0);
    pad1->SetBorderSize(5);
    pad1->SetTopMargin(0.1);
    pad1->SetRightMargin(0.1);
    pad1->SetLeftMargin(0.15);
    pad1->SetBottomMargin(0.0);

    TH1D* hback1 = new TH1D( "hback1", (char*) 0, 1, xmin, xmax );
    hback1->SetMinimum( ymin1 );
    hback1->SetMaximum( ymax1 );
    // x-axis
    hback1->GetXaxis()->SetMoreLogLabels();
    hback1->GetXaxis()->SetRangeUser( xmin, xmax );
    // y-axis
           hback1->GetYaxis()->SetMoreLogLabels();
    if ( yTitle1 != "" ) hback1->GetYaxis()->SetTitle( yTitle1 );
    hback1->GetYaxis()->SetTitleFont(42);
    hback1->GetYaxis()->SetTitleSize(0.07);
    hback1->GetYaxis()->SetTitleOffset(1.0);
    hback1->GetYaxis()->SetLabelSize(0.06);
    hback1->GetYaxis()->SetDecimals();
    hback1->SetStats(kFALSE);
    hback1->Draw("AXIS");

    TVirtualPad* pad2 = canvas->cd(2);
    pad2->SetPad(0.0, 0.0, 1.0, 0.4);
    pad2->GetFrame()->SetBorderMode(0);
    pad2->SetTopMargin(0);
    pad2->SetRightMargin(0.1);
    pad2->SetLeftMargin(0.15);
    pad2->SetBottomMargin(0.21);

   TH1D* hback2 = new TH1D( "hback2", (char*) 0, 1, xmin, xmax );
    hback2->SetMinimum( ymin2 );
    hback2->SetMaximum( ymax2 );
    // x-axis
           hback2->GetXaxis()->SetMoreLogLabels();
    if ( xTitle != "" ) hback2->GetXaxis()->SetTitle( xTitle );
    hback2->GetXaxis()->SetTitleSize(0.09);
    hback2->GetXaxis()->SetTitleOffset(1.1);
    hback2->GetXaxis()->SetLabelSize(0.09);
    hback2->GetXaxis()->SetTickLength(0.055);
    hback2->GetXaxis()->SetLabelOffset(0.015);
    hback2->GetXaxis()->SetRangeUser( xmin, xmax );
    hback2->GetXaxis()->SetNoExponent();
    // y-axis
           if ( yTitle2 != "" ) hback2->GetYaxis()->SetTitle( yTitle2 );
    hback2->GetYaxis()->SetNdivisions(nDivisions);
    hback2->GetYaxis()->SetTitleFont(42);
    hback2->GetYaxis()->SetTitleSize(0.09);
    hback2->GetYaxis()->SetTitleOffset(0.7);
    hback2->GetYaxis()->CenterTitle();
    hback2->GetYaxis()->SetLabelSize(0.09);
    hback2->GetYaxis()->SetDecimals();
    hback2->SetStats(kFALSE);
    hback2->Draw("AXIS");
  } // PrepareTwoPadCanvas()

// ___________________________________________________________

void PlottingTools::PrepareOnePadCanvas( TCanvas* canvas,
                                         TString xTitle, TString yTitle,
                                         float xmin, float xmax,
                                         float ymin, float ymax,
                                         bool setLogX,  bool setLogY ) 
{
  TVirtualPad* pad = canvas->cd(1);
  pad->Clear();
  pad->SetPad(0.0, 0.0, 1.0, 1.0);
  pad->GetFrame()->SetBorderMode(0);
  pad->SetBorderSize(5);
  pad->SetTopMargin(0.1);
  pad->SetRightMargin(0.1);
  pad->SetLeftMargin(0.15);
  pad->SetBottomMargin(0.15);
  if ( setLogX ) {
    if ( xmin == 0.0 ) xmin = 1.0;
    pad->SetLogx();
  }
  if ( setLogY ) {
    if ( ymin == 0.0 ) ymin = 1.0;
    pad->SetLogy();
  }

  TH1D* hback = new TH1D( "hback", (char*) 0, 1, xmin, xmax );
  hback->SetMinimum( ymin );
  hback->SetMaximum( ymax );
  // x-axis
  hback->GetXaxis()->SetMoreLogLabels();
  if ( xTitle != "" ) hback->GetXaxis()->SetTitle( xTitle );
  hback->GetXaxis()->SetTitleSize(0.05);
  hback->GetXaxis()->SetTitleOffset(1.2);
  hback->GetXaxis()->SetLabelSize(0.05);
  hback->GetXaxis()->SetTickLength(0.03);
  hback->GetXaxis()->SetLabelOffset(0.01);
  hback->GetXaxis()->SetRangeUser( xmin, xmax );
  hback->GetXaxis()->SetNoExponent();
  // y-axis
  hback->GetYaxis()->SetMoreLogLabels();
  if ( yTitle != "" ) hback->GetYaxis()->SetTitle( yTitle );
  hback->GetYaxis()->SetTitleFont(42);
  hback->GetYaxis()->SetTitleSize(0.05);
  hback->GetYaxis()->SetTitleOffset(1.4);
  hback->GetYaxis()->SetLabelSize(0.05);
  hback->GetYaxis()->SetDecimals();
  hback->SetStats(kFALSE);
  hback->Draw("AXIS");
} // PrepareOnePadCanvas()

// ___________________________________________________________

TGraphErrors* PlottingTools::MakeGraphFromHisto(TH1D *histo, TString name, bool ignoreYerrors, bool ignoreXerrors, bool abs, double scale)
  {
    if (!histo) return 0;

    int nbinsx = histo -> GetNbinsX();

    TGraphErrors *graph = new TGraphErrors();
    graph -> SetName(name);
    graph -> SetTitle(histo -> GetTitle());
    graph -> GetXaxis() -> SetTitle(histo -> GetXaxis() -> GetTitle());
    graph -> GetYaxis() -> SetTitle(histo -> GetYaxis() -> GetTitle());
    CopyHistoStyle(histo, graph);

    for (int i = 0; i < nbinsx; ++i) {
      graph -> SetPoint(i, histo -> GetBinCenter(i+1), fabs(histo -> GetBinContent(i+1))*scale);
      double xe = 0.;
      double ye = 0;
      if (!ignoreYerrors) ye = histo -> GetBinError(i+1);
      if (!ignoreXerrors) xe = 0.5*(histo -> GetBinWidth(i+1));
      graph -> SetPointError(i, xe, ye  );
    }
    return graph;
  }

// ___________________________________________________________

TH1D* PlottingTools::MakeHistoFromGraph(TGraphErrors *graph, TString name, bool ignoreYerrors, bool ignoreXerrors, bool abs, double scale)
  {
    TH1D *histo = 0;
    if (graph) {

      int np = graph -> GetN();
      double *binsx = new double[np+1];
      double x, y, ex, ey;

      for (int i = 0; i < np; ++i) {
        graph -> GetPoint(i, x, y);
        ex = graph -> GetErrorX(i);
        binsx[i] = x - ex;
      }
      binsx[np] = x + ex;

      histo = new TH1D(name, name, np, binsx);

      for (int i = 0; i < np; ++i) {
        graph -> GetPoint(i, x, y);
        ex = graph -> GetErrorX(i);
        ey = graph -> GetErrorY(i);
        histo -> SetBinContent(i+1, y);
        histo -> SetBinError(i+1, ey);
      }
      histo -> Scale(1.);

    } // if graph

    return histo;
  }

// ___________________________________________________________
     
void PlottingTools::SetGraphStyle(TGraph* gr, int mstyle, float msize, int mcol, int lstyle, int lcol)
{
  if (!gr) return;

  gr -> SetMarkerStyle(mstyle);
  gr -> SetMarkerSize(msize);
  gr -> SetMarkerColor(mcol);
  gr -> SetLineStyle(lstyle);
  gr -> SetLineWidth(2);
  gr -> SetLineColor(lcol);
  return;
}

// ___________________________________________________________

void PlottingTools::CopyHistoStyle(TH1D *h1, TH1D *h2)
{
  if (h1 && h2) {
    h2 -> SetLineColor(h1 -> GetLineColor());
    h2 -> SetLineStyle(h1 -> GetLineStyle());
    h2 -> SetLineWidth(h1 -> GetLineWidth());
    h2 -> SetMarkerColor(h1 -> GetMarkerColor());
    h2 -> SetMarkerStyle(h1 -> GetMarkerStyle());
    h2 -> SetMarkerSize(h1 -> GetMarkerSize());
  }
}

void PlottingTools::CopyHistoStyle(TH1D *h1, TGraph *h2)
{
  if (h1 && h2) {
    h2 -> SetLineColor(h1 -> GetLineColor());
    h2 -> SetLineStyle(h1 -> GetLineStyle());
    h2 -> SetLineWidth(h1 -> GetLineWidth());
    h2 -> SetMarkerColor(h1 -> GetMarkerColor());
    h2 -> SetMarkerStyle(h1 -> GetMarkerStyle());
    h2 -> SetMarkerSize(h1 -> GetMarkerSize());
  }
}


void PlottingTools::CopyHistoStyle(TGraph *h1, TH1D *h2)
{
  if (h1 && h2) {
    h2 -> SetLineColor(h1 -> GetLineColor());
    h2 -> SetLineStyle(h1 -> GetLineStyle());
    h2 -> SetLineWidth(h1 -> GetLineWidth());
    h2 -> SetMarkerColor(h1 -> GetMarkerColor());
    h2 -> SetMarkerStyle(h1 -> GetMarkerStyle());
    h2 -> SetMarkerSize(h1 -> GetMarkerSize());
  }
}

// ___________________________________________________________

void PlottingTools::PrintLatex( char* text,
  				double xpos, double ypos,
   				double xmax, double xmin, 
				double ymax, double ymin)
{
  TLatex *l = new TLatex(xpos*(xmax-xmin)+xmin,ypos*(ymax-ymin)+ymin, text);
  l->SetTextFont(42);
  l->SetTextColor(1);
  l->SetTextSize(0.04);
  l->Draw("same");
}



// ___________________________________________________________
void PlottingTools::ResponseVs( TH2D* hist2d, TCanvas* binnedRespCan, TGraphErrors* RespVs, char *prefix, char *variable, bool isdPhi, double minNeff, double minNeffFit)

{

  TGraphErrors* ResolutionVs = new TGraphErrors();
  ResponseVs(hist2d, binnedRespCan, RespVs, ResolutionVs, prefix, variable, isdPhi, minNeff, minNeffFit);

}


// ___________________________________________________________

void PlottingTools::ResponseVs( TH2D* hist2d, 
                                TCanvas* binnedRespCan, 
                                TGraphErrors* RespVs, 
                                TGraphErrors* ResolutionVs, 
                                char *prefix, 
                                char *variable, 
                                bool isdPhi, 
                                double minNeff, 
                                double minNeffFit)

{
  // ////////////////////////////////////////////////
  // The JES fitting tool which should be used in all 
  // JES analysis.
  // /////////////////////////////////////////////////
  JES_BalanceFitter *_JESfitter = new JES_BalanceFitter(1.6);
  vector<double> Bins;
  vector<double> _fitMean;
  vector<double> _fitError;
  vector<double> _fitSigma;
  vector<double> _fitSigmaError;
  char _namebuffer[100];
  char _titlebuffer[100];

  // ////////////////////////////////////////////////
  // Min number of events required to attempt a fit. 
  // Use mean for min_N_eff < n <= min_N_eff_for_fit, 
  // -999 otherwise
  // ////////////////////////////////////////////////
  double _min_N_eff = minNeff;
  double _min_N_eff_for_fit = minNeffFit;

  // ///////////////////////////////////////////////
  // Read bins form the histogram itself instead of
  // having to input them.
  // ///////////////////////////////////////////////
   
  for(int i=1;i<=hist2d->GetXaxis()->GetNbins()+1;++i) {
    Bins.push_back(hist2d->GetXaxis()->GetBinLowEdge(i));
  }

  // //////////////////////////////////////////////////
  // Determin layout of pads needed to show all Pt bins
  // //////////////////////////////////////////////////

  int nx = int(sqrt(hist2d->GetXaxis()->GetNbins()));
  int ny = int(sqrt(hist2d->GetXaxis()->GetNbins()));
  for ( int c = 0; nx*ny < hist2d->GetXaxis()->GetNbins(); c++ ) {
    if ( c%2 == 0 ) ny++;
    else if ( c%2 == 1 ) nx++;
  }
  binnedRespCan->Divide( nx, ny, 0.005, 0.005 );
  for(int i=0;i<=hist2d->GetXaxis()->GetNbins()-1;++i){
    binnedRespCan->cd(i+1);
    //gPad-> SetLogy();
    sprintf( _namebuffer, "%s_bin_%i", prefix, i);
    gStyle -> SetOptFit(0);
    gStyle -> SetOptStat(0);
    TH1D* _hist = hist2d->ProjectionY( _namebuffer, i+1, i+1, "e" );
    sprintf( _titlebuffer, "%.2f < %s < %.2f GeV", Bins[i], variable, Bins[i+1] );
    _hist->SetTitle( _titlebuffer );
    //_hist->SetTitleOffset(2);
    _hist->GetXaxis()->SetTitle("R_{MPF}");
    //_hist->GetXaxis()->SetTitleSize(0.09);
    char *s = strstr(prefix, "Balance"); 
    if (s != NULL)  _hist->GetXaxis()->SetTitle("R_{Bal}");
    _hist->SetMarkerColor(1);
    _hist->SetMarkerSize(0.3);
    _hist->SetMarkerStyle(20);
    _hist->SetLineColor(1);


    // Very temporary, just for early mapping plots
/*    _hist->GetXaxis()->SetTitle("p_{T}^{JES+GSC}");
    _hist->Rebin(5);
    if (Bins[i]>=30) _hist->Rebin(2);
    if (Bins[i]>=45) _hist->Rebin(2);
    if (Bins[i]>=110) _hist->Rebin(2);
    //if (Bins[i]>=160) _hist->Rebin(2);
    _hist->GetXaxis()->SetRangeUser(Bins[i]*0.25, Bins[i+1]*1.5);
 */

    _hist->Draw();

    // Make labels bigger on one plot to make it easier to read. 
    if(i==0){
       _hist->GetXaxis()->SetLabelSize(0.07);
       _hist->GetXaxis()->SetLabelOffset(0.001);
       _hist->GetXaxis()->SetTitleSize(0.07);
       _hist->GetXaxis()->SetTitleOffset(0.5);
       _hist->GetYaxis()->SetLabelSize(0.07);
       _hist->GetYaxis()->SetLabelOffset(0.001);
       _hist->GetYaxis()->SetTitleSize(0.07);
       _hist->GetYaxis()->SetTitleOffset(0.65);
       _hist->GetYaxis()->SetTitle("Events");
 //      _hist->GetXaxis()->SetRangeUser(0, 2);
       _hist->GetXaxis()->SetNdivisions(505);
    }
//    if (i==1)_hist->GetXaxis()->SetRangeUser(0, 2);


    double N_eff = pow(_hist->GetSum(),2) / _hist->GetSumw2()->GetSum();
    double mu(0.0),muError(0.0);
    double sigma(0.0),sigmaError(0.0);
    cout<<"Number of events in histo   ="<<N_eff<<endl;
    if ( N_eff >= _min_N_eff ) {
      double mean = _hist->GetMean();
      double meanError = _hist->GetMeanError(); //rms / sqrt( N_eff );
      double Sigma = _hist->GetRMS();
      double SigmaError = _hist->GetRMSError();
      mu = mean; muError = meanError;
      sigma = Sigma; sigmaError = SigmaError;
      if (N_eff >= _min_N_eff_for_fit){

// Messing around
//        _JESfitter -> SetPoisson();

        _JESfitter -> SetGaus();
        if (s != NULL){
          _JESfitter -> SetPoisson();
          _JESfitter -> FitAndDraw(_hist, (10/Bins[i-1]));
        }
        else  _JESfitter -> FitAndDraw(_hist, -1);

// Messing around
//        else _JESfitter -> FitAndDraw(_hist, 17.5);

        mu = _JESfitter -> GetMean();
        muError = _JESfitter -> GetMeanError();
        sigma = _JESfitter -> GetSigma();
        sigmaError = _JESfitter -> GetSigmaError();
      }
     
      _fitMean.push_back(mu);
      _fitError.push_back(muError);
      _fitSigma.push_back(sigma);
      _fitSigmaError.push_back(sigmaError);
    }
    else {
      _fitMean.push_back(0.);
      _fitError.push_back(-0.);
//Not sure about the two following lines (Cecilia)
      _fitSigma.push_back(0.);
      _fitSigmaError.push_back(-0.);
    }
    sprintf( _titlebuffer, "%.0f < %s < %.0f GeV", Bins[i], variable, Bins[i+1] );
//    _hist->SetTitle( _titlebuffer );
    TLatex *tex = new TLatex(); 
    tex->SetNDC(); 
    tex->SetTextFont(42);
    tex->SetTextSize(0.04);
    tex->SetTextColor(kBlack); tex->DrawLatex(0.65,0.85,_titlebuffer);

  }


  int n = 0;
  for(vector<double>::iterator it = _fitMean.begin(); it != _fitMean.end(); ++it,++n) {
    if ( fabs((*it)) > 0.001){
      if (not isdPhi){
        RespVs->SetPoint( n, (Bins[n] + Bins[n+1]) / 2, (*it) );
        //cout<<"InRespVs     , at n = "<<n<<" mean        is "<<(*it)<<endl;
      }else {
        RespVs->SetPoint( n, 3.15-(Bins[n] + Bins[n+1]) / 2, (*it) );
      }
      RespVs->SetPointError( n, (Bins[n+1] - Bins[n])/2, _fitError[n] );
    }
  }

  // Filling resolution histogram
  n = 0;
  for(vector<double>::iterator it = _fitSigma.begin(); it != _fitSigma.end(); ++it,++n) {
    if ( fabs((*it)) > 0.001){
      double thismean = _fitMean.at(n);
      double thissigma = *it;
      double thissigmaerr = _fitSigmaError.at(n);
      double thismeanerr = _fitError.at(n);

      if (not isdPhi){
        ResolutionVs->SetPoint( n, (Bins[n] + Bins[n+1]) / 2, thissigma/thismean );
       // cout<<"InResolutionVs, at n = "<<n<<" mean       is "<<thismean<<endl;
       // cout<<"InResolutionVs, at n = "<<n<<" sigma      is "<<thissigma<<endl;
       // cout<<"InResolutionVs, at n = "<<n<<" resolution is "<<thissigma/thismean<<endl;

      }
      else{ 
        ResolutionVs->SetPoint( n, 3.15-(Bins[n] + Bins[n+1]) / 2, thissigma/thismean );
      // Propagating error
      }
      double thiserror = (thissigmaerr * thissigmaerr) / (thismean * thismean);
      //thiserror += (thissigma * thissigma) / (pow(thismean,4)) * (thismeanerr * thismeanerr);
      thiserror = sqrt(thiserror); 
      ResolutionVs->SetPointError( n, (Bins[n+1] - Bins[n])/2, thiserror);
    }
  }

}  // ResponseVs

// ___________________________________________________________


TF1* PlottingTools::PlotConstantLine( float y, float xmin, float xmax, int lineType, int lineWidth ) 
{
  TF1* f = new TF1( "f", "[0]", xmin, xmax );
  f->SetLineStyle(lineType);
  f->SetLineColor(1);
  f->SetLineWidth(lineWidth);
  f->SetParameter( 0, y );
  return f;
}

// _____________________________________________________________

TLine *PlottingTools::MakeTLine(double x1, double y1, double x2, double y2, int col) {
  TLine *line = new TLine(x1, y1, x2, y2);
  line -> SetLineColor(col);
  //line -> SetLineWidth(3);
  return line;
}

// _____________________________________________________________
TF1 PlottingTools::FitResponse( TGraphErrors* g, double xmin, double xmax,
                 bool usePWR, bool useLOG2, bool useLOG3, bool useLowETbiasTerm ) {

  const double scale = 100.0;
  TF1 fit;
  if ( useLOG2 || useLOG3 ) {
    fit = TF1( "RLOG", Wigmans, 7.0, 1000.0, 7 );
    //fit = TF1( "RLOG", "( [0] + [1]*log(x/[4]) + [2]*pow(log(x/[4]),2) + [3]*pow(log(x/[4]),3) ) * ( 1 + [5]*[6]*sqrt(2/TMath::Pi())/x* )", 7.0, 1000.0 );
    fit.SetParNames("a","b","c","d","E_{scale}","useLowETbiasTerm","resolution");
    fit.SetParameter(0,0.7);
    fit.FixParameter(4,scale);
    if ( useLOG2 ) fit.FixParameter(3,0.0);
    if ( useLowETbiasTerm ) {
      fit.FixParameter(5,1.0);
      fit.SetParLimits(6,0.0,100.0);
    }
    else {
      fit.FixParameter(5,-1.0);
      fit.FixParameter(6,0.0);
    }
    fit.SetLineStyle(kSolid);
    fit.SetLineWidth(2);
  }
  else if ( usePWR ) {
    fit = TF1( "RPWR", Groom, 7.0, 1000.0, 5 );
    fit.SetParNames("b","m","E_{scale}","useLowETbiasTerm","resolution");
    fit.SetParameter(0,-0.33);
    fit.SetParameter(1,0.3);
    fit.SetParLimits(0,-1.0e06,0.0);   // b < 0 
    fit.SetParLimits(1,0.1,1.0);       // 1/2 < m < 1
    fit.FixParameter(2,scale);
    if ( useLowETbiasTerm ) {
      fit.FixParameter(3,1.0);
      fit.SetParLimits(4,0.0,100.0);
    }
    else {
      fit.FixParameter(3,-1.0);
      fit.FixParameter(4,0.0);
    }
    fit.SetLineStyle(kSolid);
    fit.SetLineWidth(2);
  }
  g->Fit( &fit, "MEEX0S", "", xmin, xmax );

  if ( useLOG2 ) cout << "LOG2: {";
  if ( useLOG3 ) cout << "LOG3: {";
  if ( usePWR  ) cout << "PWR: {";
  for ( int n = 0; n < fit.GetNpar(); ++n ) {
   // cout << fit.GetParameter(n) << " +/- " << fit.GetParError(n);
    //if ( n < fit.GetNpar() - 1 ) cout << " , ";
  }
  //cout << " }; " << endl;
  return fit;
} // FitResponse()

// _____________________________________________________________

double PlottingTools::Wigmans( double* x, double* p ) {

  double E = x[0];
  double scale = p[4];
  double R = 0.0;
  for ( int i = 0; i <= 3; ++i )
    R += p[i]*pow( log( E/scale ),i);
  bool useLowETbiasTerm = p[5] > 0 ? true : false;
  double lowETbias = 0.0;
  if ( useLowETbiasTerm ) {
    double resolution = p[6];
    double Ethr = 7; // GeV
    double Erf = TMath::Erf( ( Ethr - E ) / (sqrt(2)*resolution) );
    lowETbias = resolution * sqrt(2/TMath::Pi()) * ( R / E ) * exp( - 0.5 * pow(( Ethr - E ),2) / pow(resolution,2) ) / ( 1.0 - Erf );
  }
  return ( R + lowETbias );
}

// ________________________________________________________________
double PlottingTools::Groom( double* x, double* p ) {

  double E = x[0];
  double scale = p[2];
  double R = 1.0 + p[0]*pow( ( E/scale ),(p[1]-1.0));
  bool useLowETbiasTerm = p[3] > 0 ? true : false;
  double lowETbias = 0.0;
  if ( useLowETbiasTerm ) {
    double resolution = p[4];
    double Ethr = 7; // GeV
    double Erf = TMath::Erf( ( Ethr - E ) / (sqrt(2)*resolution) );
    lowETbias = resolution * sqrt(2/TMath::Pi()) * ( R / E ) * exp( - 0.5 * pow(( Ethr - E ),2) / pow(resolution,2) ) / ( 1.0 - Erf );
  }
  return ( R + lowETbias );
}

// ______________________________________________________________
TH1D *PlottingTools::RemoveFirstPoint(TH1D *h1){
  vector<double> ptBins;
  for(int i=2;i<=h1->GetXaxis()->GetNbins()+1;++i) {ptBins.push_back(h1->GetXaxis()->GetBinLowEdge(i));}
  TH1D* tmp = new TH1D(h1->GetName(), h1->GetTitle(), h1->GetXaxis()->GetNbins()-1, (&(*(ptBins.begin()))));
  for(int i=2;i<=h1->GetXaxis()->GetNbins()+1;++i) {tmp->SetBinContent(i-1, h1->GetBinContent(i)); tmp->SetBinError(i-1, h1->GetBinError(i));}
  return tmp;
}

// ______________________________________________________________
TGraphErrors *PlottingTools::GetResponse(TFile *file, TString name){
  char _namebuffer[100];
  sprintf( _namebuffer, name);
  TGraphErrors* graph1 = new TGraphErrors();
  TGraphErrors* graph2 = new TGraphErrors();
  sprintf( _namebuffer, name);
  graph1->SetName(_namebuffer);
  graph2->SetName("Resolution_Test");
  TCanvas* can = new TCanvas();
  TH2D* hist2d = (TH2D*)file->Get("hResponseProbePT");
  char variable[40];
  sprintf(variable, "p_{T}^{Ref}");
  char prefix[40];
  sprintf( prefix, "Response");
  ResponseVs( hist2d, can, graph1, graph2, prefix, variable, false);
  return graph1;
  return graph2;
}
