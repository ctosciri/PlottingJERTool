#include "PlottingTools/PurityTools.h"
#include "PlottingTools/PlottingTools.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TLine.h>
#include <TProfile.h>

using namespace std;
using namespace PurityTools;
using namespace PlottingTools;


int main(int argc, char **argv) {
  SetAtlasStyle();

  char _namebuffer[100];
  vector<double> VarBins;
  vector<double> RefPtBins;

  TString _filename   = "../Data_EM_4Dec";

  TString _mcFilename ="../Pythia_EM_4Dec";
  bool doCorr = false;
 
  int ip=1;
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _filename=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno histogram name specified"<<endl; break;}
      }
      else if (string(argv[ip])=="--MC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _mcFilename=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno histogram name specified"<<endl; break;}
      }
    }
    /////////////////////////////////////////////////
    // If no options specified assume there's only
    // one argument and that argument is the filename
    /////////////////////////////////////////////////
    else {
      cout << argv[ip] << endl;
      cout << "Arguments must be specified using --file" << endl;
      cout << "Since this was not done assuming the first argument given is the filename" << endl;
      _filename=argv[ip];
      ip=argc;
    }
  }

  double xMin=10;
  double xMax=2000;

  TGraphErrors *purity_gr         = new TGraphErrors("purity",       "purity");
  TGraphErrors *purity_gr_corr    = new TGraphErrors("purityCorr",   "purityCorr");
  TGraphErrors *purity_gr_corr_BG = new TGraphErrors("purityCorrBG", "purityCorrBG");

  TString outname = _filename;
  outname.ReplaceAll(".root", "_ResponseVsPt_bins_LNT.pdf");

  if (_mcFilename != "") doCorr = true;
  TFile *_mcInfile = 0;
  if (doCorr) _mcInfile = TFile::Open(_mcFilename, "UPDATE");
  TFile *_infile = TFile::Open(_filename, "UPDATE");

//  if (_mcFilename != "") doCorr = true;
//  TFile *_mcInfile = 0;
//  if (doCorr) _mcInfile = TFile::Open(_mcFilename, "UPDATE");


/*  const float _phptbins[]  =  { 25, 45, 65, 85, 105, 125, 160, 210, 260, 310, 400, 500, 600, 800};
  const float _x1[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  const float _x2[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
*/
  const float _phptbins[]={25., 45., 65., 85., 105., 125., 160., 210., 260., 310., 400., 500., 600., 800., 1000, 1200, 1400, 1600, 2000, 10000};


  const int _nphptbins  = ( sizeof(_phptbins)  / sizeof(_phptbins[0]) ) - 1;

  TH2D* hist2d = (TH2D*)_infile->Get( "hResponseLNTProbePT" );


  TCanvas* RespVsPtLNTCan = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
  RespVsPtLNTCan->Divide( 1, 2, 0.0, 0.01, 0 );
  RespVsPtLNTCan->cd(1);
  gPad -> SetLogx();
  RespVsPtLNTCan->cd(2);
  gPad -> SetLogx();

  PrepareTwoPadCanvas( RespVsPtLNTCan, "P_{T}^{Ref}", "R_{MPF}", "LNT/Nominal", xMin, xMax, 0.35001, 1.19001, 0.8001, 1.199, 505);

  sprintf( _namebuffer, "Response_distributions_in_PRef_bins");
  TCanvas* binnedRespVsPtLNTCan = new TCanvas( _namebuffer, _namebuffer );

  TGraphErrors* RespVsPtLNT = new TGraphErrors();
  sprintf( _namebuffer, "ResponseVsProbePT_graph" );
  RespVsPtLNT->SetName(_namebuffer);
  RespVsPtLNT->SetTitle(_namebuffer);

  TGraphErrors* RespVsPt = (TGraphErrors*)_infile->Get("ResponseVsProbePT_graph");


  char variable[40];
  sprintf(variable, "p_{T}^{Ref}");
  ResponseVs( hist2d, binnedRespVsPtLNTCan, RespVsPtLNT, "Response", variable, false);
  sprintf( _namebuffer, "Photon_Data_LC_ResponseVsPt_bins_LNT.pdf");
  binnedRespVsPtLNTCan->SaveAs(_namebuffer);


  RespVsPtLNTCan->cd(1);
  
  RespVsPtLNT->SetMarkerStyle(20);
  RespVsPtLNT->SetMarkerSize(1.2);
  RespVsPtLNT->SetMarkerColor(4);
  RespVsPtLNT->SetLineStyle(1);
  RespVsPtLNT->SetLineWidth(1);
  RespVsPtLNT->SetLineColor(4);
  RespVsPtLNT->Draw("sameP");
  RespVsPt->Draw("sameP");

  TLegend *leg1 = new TLegend(0.5, 0.07, 0.85, 0.15);
  leg1->SetBorderSize(0);
  leg1->SetFillColor(0);
  leg1->AddEntry(RespVsPt, "Tight", "PL");
  leg1->AddEntry(RespVsPtLNT, "Loose not Tight", "PL");
  leg1->Draw();

  RespVsPtLNTCan->cd(2);
  TGraphErrors* gRatio = new TGraphErrors();
  gRatio->SetMarkerStyle(20);
  gRatio->SetMarkerSize(1.0);
  gRatio->SetMarkerColor(4);
  for ( int n = 0; n < RespVsPtLNT->GetN()-2; ++n ) {
    if (RespVsPtLNT->GetY()[n] == 0 or RespVsPt->GetY()[n]==0) continue;
    if (RespVsPtLNT->GetY()[n] == -999 or RespVsPt->GetY()[n]==-999) continue;
    gRatio->SetPoint(n, 0.5*(RespVsPtLNT->GetX()[n] + RespVsPt->GetX()[n]), RespVsPtLNT->GetY()[n] / RespVsPt->GetY()[n]);
    double yerr = RespVsPtLNT->GetY()[n] / RespVsPt->GetY()[n] * sqrt( pow(RespVsPtLNT->GetEY()[n]/RespVsPtLNT->GetY()[n],2) + pow(RespVsPt->GetEY()[n]/RespVsPt->GetY()[n],2) );
    gRatio->SetPointError(n, RespVsPtLNT->GetEX()[n], yerr);
  }

  TF1 *LineAtCenter = PlotConstantLine( 1.0 , xMin, xMax, 2, 1 );
  TF1 *LineAtMinus  = PlotConstantLine( 0.95, xMin, xMax, 3, 1 );
  TF1 *LineAtPlus   = PlotConstantLine( 1.05, xMin, xMax, 3, 1 );

  LineAtCenter->Draw("sameL");
  LineAtMinus->Draw("sameL");
  LineAtPlus->Draw("sameL");

  gRatio->Draw("sameP");

  sprintf( _namebuffer, "ResponseLNTVsPt.pdf");
  RespVsPtLNTCan->SaveAs(_namebuffer);



  for (int i = 0; i < _nphptbins; i++) {
    string name = "IsEMIsolCone_" + to_string((int)_phptbins[i]) + "_to_" + to_string((int)_phptbins[i+1]) + "_GeV";
    TString _histname = TString(name);
    TCanvas *can = new TCanvas(name.c_str(), name.c_str(), 200, 200, 1400, 1000);

    TH2D* IsolID = (TH2D*)_infile->Get(_histname);
    IsolID->Print("all");   
 
    TH2D* MCIsolID =0;
    if (doCorr) MCIsolID = (TH2D*)_mcInfile->Get(_histname);

    TProfile *profile = (TProfile*) IsolID->ProfileX(Form("profile_%i_%i", 1, i));
    profile -> SetLineColor(kBlack);
    profile -> SetMarkerColor(kBlack);
    profile -> SetMarkerStyle(2);

    TH1D *projection = (TH1D*) IsolID->ProjectionX(Form("projection_%i_%i", 1, i));
    int profcol = 1;
    projection -> SetLineColor(profcol);
    projection -> SetMarkerColor(profcol);
    projection -> SetMarkerStyle(20);
    double Integral = projection -> Integral();
    if (Integral > 0.) projection -> Scale(8./Integral);

    TH1D *projectionTight = (TH1D*) IsolID->ProjectionX(Form("projectionTight_%i_%i", 1, i), 2, 2);
    profcol = 1;
    projectionTight->SetLineColor(profcol);
    projectionTight->SetMarkerColor(profcol);
    projectionTight->SetMarkerStyle(20);
    Integral = projectionTight->Integral(projectionTight->FindBin(1), -1);
    if (Integral > 0.) projectionTight->Scale(4./Integral);
    TH1D *projectionLNT = (TH1D*) IsolID->ProjectionX(Form("projectionLNT_%i_%i", 1, i), 1, 1);
    profcol = 2;
    projectionLNT->SetLineColor(profcol);
    projectionLNT->SetMarkerColor(profcol);
    projectionLNT->SetMarkerStyle(21);
    Integral = projectionLNT->Integral(projectionLNT->FindBin(1), -1);
    if (Integral > 0.) projectionLNT->Scale(4./Integral);


//    IsolID->GetYaxis()->SetBinLabel(1, "LNT");
//    IsolID->GetYaxis()->SetBinLabel(2, "Tight");
    IsolID->GetYaxis()->SetTitle("A.U");
    IsolID->GetXaxis()->SetTitle("topoetcone40");

    IsolID->Draw("Axis");
    TLegend *leg3 = new TLegend(0.5, 0.67, 0.85, 0.80);
    leg3 -> SetBorderSize(0);
    leg3 -> SetFillColor(0);
    leg3 -> AddEntry(projectionTight, "Tight", "PL");
    leg3 -> AddEntry(projectionLNT, "Loose not tight", "PL");
    leg3->Draw();

    projectionTight->Draw("e2histsame");
    projectionLNT->Draw("e2histsame");
    can->SaveAs(TString(name+"_ditributions.png"));

    can->Clear();
    IsolID->GetXaxis()->SetTitle("");
    IsolID->GetYaxis()->SetBinLabel(1, "LNT");
    IsolID->GetYaxis()->SetBinLabel(2, "Tight");
    IsolID->GetYaxis()->SetLabelSize(0.06);
    IsolID->GetXaxis()->SetTitle("TopoEtcone40");
    IsolID->GetXaxis()->SetTitleSize(0.04);
    IsolID->Draw("colz");
//    profile -> Draw("e2same");
//    projection-> Draw("e2histsame");
//    projectionTight->Draw("e2histsame");
//    projectionLNT->Draw("e2histsame");


    TLine *line1x = MakeTLine(1, IsolID->GetYaxis()->GetXmin(), 1, IsolID->GetYaxis()->GetXmax() );
    line1x -> SetLineWidth(3);
    line1x -> Draw();
    TLine *line2x = MakeTLine(1, IsolID->GetYaxis()->GetXmin(), 1, IsolID->GetYaxis()->GetXmax() );
    line2x -> SetLineWidth(3);
    line2x -> Draw();
    TLine *line1y = MakeTLine( IsolID->GetXaxis()->GetXmin(), 1, IsolID->GetXaxis()->GetXmax(), 1 );
    line1y -> SetLineWidth(3);
    line1y -> Draw();
    TLine *line2y = MakeTLine( IsolID->GetXaxis()->GetXmin(), 1, IsolID->GetXaxis()->GetXmax(), 1 );
    line2y ->SetLineWidth(3);
    line2y -> Draw();

    TLatex *Atext = new TLatex(0.5*(1 + IsolID ->GetXaxis() -> GetXmin()), 0.5*(1 + IsolID ->GetYaxis() -> GetXmax()), "A");
    Atext -> SetTextColor(2); Atext -> Draw();

    TLatex *Btext = new TLatex(0.5*(1 + IsolID ->GetXaxis() -> GetXmax()), 0.5*(1 + IsolID ->GetYaxis() -> GetXmax()), "B");
    Btext -> SetTextColor(2); Btext -> Draw();

    TLatex *Ctext = new TLatex(0.5*(1 + IsolID ->GetXaxis() -> GetXmin()), 0.5*(0.5*1 + 0), "C");
    Ctext -> SetTextColor(2); Ctext -> Draw();

    TLatex *Dtext = new TLatex(0.5*(1 + IsolID ->GetXaxis() -> GetXmax()), 0.5*(0.5*1 + 0), "D");
    Dtext -> SetTextColor(2); Dtext -> Draw();

    can->SaveAs(TString(name+".png")); 
/*    can->Clear();
    IsolID->Draw("Axis");
    projectionTight->Draw("e2histsame");
    projectionLNT->Draw("e2histsame");
    can->SaveAs(TString(name+"_ditributions.png"));
  */   
 


    vector<pair<double,double>> allresultsData = GetPurity(1, 1, 0., 1.,   IsolID);
    vector<pair<double,double>> allresultsMC   = GetPurity(1, 1, 0., 1., MCIsolID);
    // +++ get the naive purity without signal leakage to ABCD methog bkg regions:
    //pair<double,double> result = allresultsData[0];
    double A = allresultsData[1].first;
    double B = allresultsData[2].first;
    double D = allresultsData[4].first;
    double C = allresultsData[3].first;
    double AsigData = A - allresultsData[5].first;
    double purity = allresultsData[0].first;
    double error  = allresultsData[0].second;
    if (purity > 0. and (purity == purity)) {
      purity_gr->SetPoint(i, (_phptbins[i]+_phptbins[i+1])/2, purity);
      purity_gr->SetPointError(i, 0., error);
    }

    if (doCorr) {
      double AallMC = allresultsMC[1].first;
      double CorrB  = allresultsMC[2].first / AallMC;
      double CorrC  = allresultsMC[3].first / AallMC;
      double CorrD  = allresultsMC[4].first / AallMC;
 
      // R_MC, correlations between loose/tight and isol
      double R = 1; //.45;

      double a = R*CorrB*CorrC - CorrD;
      double b = D + CorrD*A - R*CorrC*B - R*CorrB*C;
      double c = R*C*B - A*D;
      double Disc = b*b - 4*a*c;
      double AsigNew = AsigData;
      //double AsigOther = AsigNew;
      if (Disc > 0) {
        double x1 = (-b + sqrt(Disc)) / (2*a);
        double x2 = (-b - sqrt(Disc)) / (2*a);
        cout << " AsigOld = " << AsigData
             << " x1 = " << x1
             << " x2 = " << x2
             << endl;
        if (x2 < x1 and x2 > 0.) {
          AsigNew = x2;
          //AsigOther = x1;
        } else {
          AsigNew = x1;
          //AsigOther = x2;
        }
      }
      double purityNew = AsigNew / A;
      if (purityNew > 1) purityNew = 1.;
      cout << "AsigNew = " << AsigNew << " , before correction: " << purity << " , after correction: " << purityNew << endl;
      if (purityNew > 0. and purityNew == purityNew) {
        purity_gr_corr -> SetPoint(i, (_phptbins[i]+_phptbins[i+1])/2, purityNew);
        purity_gr_corr -> SetPointError(i, 0., error);
      }
     
      R = 1.45;
      a = R*CorrB*CorrC - CorrD;
      b = D + CorrD*A - R*CorrC*B - R*CorrB*C;
      c = R*C*B - A*D;
      Disc = b*b - 4*a*c;
      AsigNew = AsigData;
      //AsigOther = AsigNew;
      if (Disc > 0) {
        double x1 = (-b + sqrt(Disc)) / (2*a);
        double x2 = (-b - sqrt(Disc)) / (2*a);
        cout << " AsigOld = " << AsigData
             << " x1 = " << x1
             << " x2 = " << x2
             << endl;
        if (x2 < x1 and x2 > 0.) {
          AsigNew = x2;
          //AsigOther = x1;
        } else {
          AsigNew = x1;
          //AsigOther = x2;
        }
      }
      purityNew = AsigNew / A;
      if (purityNew > 1) purityNew = 1.;
      cout << "AsigNewer = " << AsigNew << " , before correction: " << purity << " , after correction: " << purityNew << endl;
      if (purityNew > 0. and purityNew == purityNew) {
      purity_gr_corr_BG -> SetPoint(i, (_phptbins[i]+_phptbins[i+1])/2, purityNew);
      purity_gr_corr_BG -> SetPointError(i, 0., error);
      }


    }
    

  }
  cout << endl << endl;


  TString pcanname =  "PurityPlot";
  TCanvas *pcan = new TCanvas(pcanname, pcanname, 0, 0, 1000, 800);
  PrepareOnePadCanvas( pcan, "P_{T}^{Ref}", "Purity", xMin, xMax, 0.45, 1.05, true, false);
  pcan->cd();

  purity_gr -> SetMarkerStyle(20);
  purity_gr -> SetMarkerSize(1.2);
  purity_gr -> SetMarkerColor(1);
  purity_gr -> SetLineStyle(2);
  purity_gr -> SetLineWidth(1);
  purity_gr -> SetLineColor(1);
  purity_gr->Draw("PL"); 

  purity_gr_corr -> SetMarkerStyle(21);
  purity_gr_corr -> SetMarkerSize(1.2);
  purity_gr_corr -> SetMarkerColor(2);
  purity_gr_corr -> SetLineStyle(2);
  purity_gr_corr -> SetLineWidth(1);
  purity_gr_corr -> SetLineColor(2);
  purity_gr_corr->Draw("PL");
 
  purity_gr_corr->Print("all");


  purity_gr_corr_BG -> SetMarkerStyle(22);
  purity_gr_corr_BG -> SetMarkerSize(1.2);
  purity_gr_corr_BG -> SetMarkerColor(3);
  purity_gr_corr_BG -> SetLineStyle(2);
  purity_gr_corr_BG -> SetLineWidth(1);
  purity_gr_corr_BG -> SetLineColor(3);
  purity_gr_corr_BG->Draw("PL");

  TLegend *leg = new TLegend(0.5, 0.17, 0.85, 0.30);
  leg -> SetBorderSize(0);
  leg -> SetFillColor(0);
  leg -> AddEntry(purity_gr,         "Simple", "PL");
  leg -> AddEntry(purity_gr_corr,    "Signal leakage corrected", "PL");
  leg -> AddEntry(purity_gr_corr_BG, "Leakage+correlation corrected", "PL");
  leg->Draw();
  PlotConstantLine( 1.0, xMin, xMax, 3, 1 );

  pcan->Print("PurityPlots.pdf");

  TH1D *PurityHist = new TH1D("PurityHist","PurityHist",_nphptbins,_phptbins);

  cout << "Naive: ";
  for (int i = 0; i < _nphptbins; i++){
     cout << (RespVsPtLNT->GetY()[i] - RespVsPt->GetY()[i]) * (1-purity_gr->GetY()[i]) << " ";
  }
  cout << endl << " Corrected: ";
  for (int i = 0; i < _nphptbins; i++){
     cout << (RespVsPtLNT->GetY()[i] - RespVsPt->GetY()[i]) * (1-purity_gr_corr_BG->GetY()[i]) << " : "  << purity_gr_corr_BG->GetY()[i] << ", ";
     PurityHist->SetBinContent(i+1, 0.05*(1-purity_gr_corr_BG->GetY()[i])); //(RespVsPtLNT->GetY()[i] - RespVsPt->GetY()[i]) * (1-purity_gr_corr_BG->GetY()[i]));
  }
  cout << endl;
//  PurityHist->Print("all");
  PurityHist->Write("PurityHist");
//  _infile->Write();
}

