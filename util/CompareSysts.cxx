#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <TLegend.h>

#include "PlottingTools/PlottingTools.h"

using namespace PlottingTools;
using namespace std;

int main(int argc, char *argv[])
{
  int ip=1;
  int iplast = ip;
  string _dataname;
  string _mcname;
  string _binning = " ";

  SetAtlasStyle(); 
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _dataname=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--MC") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _mcname=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--binning") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _binning=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno binning specified"<<std::endl; break;}
      }
    }
    if (ip == iplast) {cout << "problem with arguments " << ip << " " << iplast <<  endl; return 1;}
    iplast = ip;
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Plotting systematic uncertainties found in " << _dataname << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
  
  TFile* _datafile = TFile::Open(_dataname.c_str(), "read");
  TFile* _mcfile   = TFile::Open(_mcname.c_str(), "read");

  vector<double>ptBins;

  TCanvas *c1 = new TCanvas();
  double xMin = 15;
  double xMax = 1100;
  double yMax = 10; //10, 6;
//  PrepareOnePadCanvas( c1, "p_{T}^{Ref} [GeV]", "Relative JES uncertainty [%]",17, 1000, 0, 15, true, false); //0.2, true, false);
  PrepareOnePadCanvas( c1, "p_{T}^{JES} [GeV]", "Relative JES uncertainty [%]",xMin, xMax, 0, yMax, true, false);
 
  TLegend *leg = new TLegend(0.7,0.55,0.88,0.85,NULL,"brNDC");
  leg->SetBorderSize(0);

 
  int colour = 2;
  int lType=1;
  bool first=true;
  int nbinsx=0;
  TH1D *h_totalEM = (TH1D*) _datafile->Get("h_total"); 
  h_totalEM->SetLineWidth(2);
  h_totalEM->SetFillColor(0);
  h_totalEM->SetLineColor(1);
  h_totalEM->Draw("C same"); // C same
  leg->AddEntry(h_totalEM, "EM scale", "l");

  TH1D *h_totalLC = (TH1D*) _mcfile->Get("h_total");
  h_totalLC->SetLineWidth(2);
  h_totalLC->SetFillColor(0);
  h_totalLC->SetLineColor(2);
  h_totalLC->Draw("C same"); // C same
  leg->AddEntry(h_totalLC, "LC scale", "l");
  
 
  leg->Draw("same");

  h_totalEM->Draw("Axis same");

  TLatex *latex = new TLatex();
  latex->SetTextColor(1);
  latex->SetTextFont(42);
  latex->SetTextSize(0.04);
  double xfrac = 0.02; //0.01
  latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.85*yMax, "#sqrt{s}=13 TeV, MPF with #gamma-jet");
  latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.77*yMax, "anti-k_{t} R=0.4, |#eta_{jet}| < 0.8");
  ATLASLabel(0.3,0.84, "Preliminary");
  c1->SaveAs("SystsCompare.C");
  c1->SaveAs("SystsCompare.pdf");
  
}

