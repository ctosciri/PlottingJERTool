#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <TLegend.h>

#include "PlottingTools/PlottingTools.h"

using namespace PlottingTools;
using namespace std;

int main(int argc, char *argv[])
{
  int ip=1;
  int iplast = ip;
  string _infilename;
  string _binning = " ";
  bool isPhoton = true;
  bool isEM=false;
  bool isPFlow=false;

  SetAtlasStyle(); 
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--infile") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _infilename=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--binning") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _binning=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno binning specified"<<std::endl; break;}
      }
    }
    if (ip == iplast) {cout << "problem with arguments " << ip << " " << iplast <<  endl; return 1;}
    iplast = ip;
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Plotting systematic uncertainties found in " << _infilename << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
  
  TFile* _infile = TFile::Open(_infilename.c_str(), "read");
  TList* list = _infile->GetListOfKeys();
  TIter next(list);
  TObject* object = 0; 

  vector<pair<string, TGraphErrors*>> Systs;
  vector<double>ptBins;

  TCanvas *c1 = new TCanvas();
  double xMin = 15;
  double xMax = 1600;
  double yMax = 90; //10, 6;
//  PrepareOnePadCanvas( c1, "p_{T}^{Ref} [GeV]", "Relative JES uncertainty [%]",17, 1000, 0, 15, true, false); //0.2, true, false);
  PrepareOnePadCanvas( c1, "p_{T}^{JES} [GeV]", "Relative JER uncertainty [%]",xMin, xMax, 0, yMax, true, false);
 
  TLegend *leg = new TLegend(0.7,0.55,0.88,0.85,NULL,"brNDC");
  leg->SetBorderSize(0);

 
  int colour = 2;
  int lType=1;
  bool first=true;
  int nbinsx=0;

  while ((object =  next())) {
    string name(object->GetName());
    TH1D* hist = (TH1D*) _infile->Get(object->GetName());
    if (first){
      nbinsx = hist->GetNbinsX()+1;
      for (int i = 0; i < nbinsx; ++i) {ptBins.push_back(hist->GetXaxis()->GetBinLowEdge(i+1));cout << hist->GetXaxis()->GetBinLowEdge(i+1) << endl;}
      first=false;
      //hist->Print("all");
    }
    if ((name.find("Zjet")!=std::string::npos) or (name.find("ZJet")!=std::string::npos)){isPhoton=false;}
    if ((name.find("LC")!=std::string::npos)){isEM=false;}
    if ((name.find("PFlow")!=std::string::npos)){isEM=false;isPFlow=true;};
    if (isPhoton and (name.find("MUONS")!=std::string::npos)) continue;
    if (!(name.find("Nominal")!=std::string::npos) and !(name.find("NoSyst")!=std::string::npos) and !(name.find("nominal")!=std::string::npos)){
//    if (!EndsWith(name,"Nominal")  and !(name.find("MET") != std::string::npos)){// and (name.find("Stat") != std::string::npos or name.find("AltMC") != std::string::npos or name.find("Purity") != std::string::npos)){
      string tmp = name;
      std::size_t found = tmp.find("_")+1;
      tmp = name.erase(0, found);
//      string tmp = name.erase(0, 14);
      //std::size_t 
      //std::size_t 
      found = tmp.find("Anti")-1;
      tmp = tmp.substr(0, found);
      found = tmp.find("__1");
      tmp = tmp.substr(0, found);


      found = tmp.find("__down");
      tmp = tmp.substr(0, found);  
      found = tmp.find("__up");
      tmp = tmp.substr(0, found);

      bool Unique = true;
      
      for (unsigned int iSyst =0 ; iSyst < Systs.size(); ++iSyst) {
        if (tmp == Systs.at(iSyst).first) Unique = false;
      }
      TGraphErrors *Error = MakeGraphFromHisto(hist, tmp, true, true, true, 100.);
      if(Unique){
        SetGraphStyle(Error, 0, 0., colour, lType, colour); //colour, colour);
  	++colour;
        if (colour == 3) ++colour;
        if (colour == 10) {colour=2; ++lType;}//++colour;
        
        // For gamma+jet since we don't have Sherpa below 35 GeV I've been told to just use the same uncertainty we measure in the next bin up (35-45 Gev)
        if (TString(tmp)=="MC") Error->SetPoint(0, Error->GetX()[0], Error->GetY()[1]);
        if (TString(tmp)=="MC" || TString(tmp)=="Stat") continue;        
 
        Systs.push_back(std::make_pair(tmp, Error));
        leg->AddEntry(Error, TString(tmp), "lp");
        cout << tmp << endl;
      } else {
	vector<pair<string, TGraphErrors*>>::const_iterator it = Systs.begin();
        for(;it!=Systs.end();++it) if ((*it).first == tmp) break;
        int N = Error->GetN();
        for (int ibin = 0; ibin < N; ++ibin) {
          double mean = (Error->GetX()[ibin] + (*it).second->GetX()[ibin] )/2;
          (*it).second->SetPoint(ibin, mean, (*it).second->GetY()[ibin]);
        }
      }
    }
  }

//  const float _Zptbins[] =  { 17., 20., 25., 30., 35., 45., 60., 80., 110., 160., 210., 260., 350., 800};
//  const int _nZptbins  = ( sizeof(_Zptbins)  / sizeof(_Zptbins[0]) ) ; //- 1;
//  const int _nZptbins = ptBins.size();


//  const float _phptbins[]=  { 25, 45, 65, 85, 105, 125, 160, 210, 260, 310, 400, 500, 600 ,800, 2000 }; 
//  const int _nphptbins  = ( sizeof(_phptbins)  / sizeof(_phptbins[0]) ) - 1; 


  TH1D *h_total = new TH1D("h_total", "h_total",nbinsx-1, (&(*(ptBins.begin()))));;
//  if (isPhoton)  h_total = new TH1D("h_total","",_nphptbins,_phptbins);
//  else           h_total = new TH1D("h_total","",_nZptbins,_Zptbins);


  double total[nbinsx+2];
  for (int i = 0; i < nbinsx; i++) total[i] = 0;

/*  double total[_nphptbins+2];
  for (int i = 0; i < _nphptbins; i++) total[i] = 0;
 */

  for (unsigned int iSyst = 0; iSyst < Systs.size(); ++iSyst) { 
    int N = Systs.at(iSyst).second->GetN();
    for (int ibin = 0; ibin <= N; ++ibin) {
      if (Systs.at(iSyst).second->GetY()[ibin] > 0.000001){
        total[ibin] += Systs.at(iSyst).second->GetY()[ibin] * Systs.at(iSyst).second->GetY()[ibin];
//	if (ibin==13)cout << Systs.at(iSyst).first << " " << ibin << " " << Systs.at(iSyst).second->GetY()[ibin] << " " << total[ibin] << endl;
      }
      
    }
  }

  for (int i = 0; i < nbinsx+1; i++) {
    h_total->SetBinContent(i+1,sqrt(total[i]));
  }
/*
  if (isPhoton){
    for (int i = 0; i < _nphptbins+2; i++) {
      h_total->SetBinContent(i+1,sqrt(total[i]));
    }
  } else {
    for (int i = 0; i < _nZptbins+2; i++) {
      h_total->SetBinContent(i+1,sqrt(total[i]));
    }
  }
*/
  h_total->SetLineWidth(2);
  h_total->SetFillColor(kBlue-10);
  h_total->SetLineColor(1);
  h_total->Draw("C same"); // C same
 
 for (unsigned int iSyst = 0; iSyst < Systs.size(); ++iSyst){
//   Systs.at(iSyst).second->RemovePoint(13);
//   Systs.at(iSyst).second->RemovePoint(12);
 
  
   Systs.at(iSyst).second->Draw("L"); //PC
  }
  
  TFile *f1 = new TFile("/afs/cern.ch/user/c/ctosciri/private/PlottingTools/output_root/output_resolution.root");//File MC JER
  TFile *f2 = new TFile("/afs/cern.ch/user/c/ctosciri/private/PlottingTools/output_root/RatioRecoTruth.root");//File Reco-Truth Ratio resolution

  TH1D* JER_Pythia = (TH1D*)f1->Get("JER_btag77_Pythia");
  TH1D* JER_Sherpa = (TH1D*)f1->Get("JER_btag77_Sherpa"); 
  TH1D* RatioRecoTruth = (TH1D*)f2->Get("RecoTruthRatio_btag77_Pythia");

  TH1D *JER_Pythia_copy = (TH1D*)JER_Pythia->Clone("JER_Pythia_copy"); 
  TH1D *Closure = (TH1D*)RatioRecoTruth->Clone("Closure");

  JER_Pythia_copy->Divide(JER_Sherpa);
  Closure->Divide(JER_Pythia);
  
  JER_Pythia_copy->Draw("same");
  Closure->Draw("same");
  leg->Draw("same");

  h_total->Draw("Axis same");

  TLatex *latex = new TLatex();
  latex->SetTextColor(1);
  latex->SetTextFont(42);
  latex->SetTextSize(0.04);
  double xfrac = 0.012; //0.01
/*  if(isPhoton)  latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.85*yMax, "#sqrt{s}=13 TeV, MPF with #gamma-jet");
  else latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.85*yMax, "#sqrt{s}=13 TeV, MPF with Z-jet");
  if (isEM)latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.77*yMax, "anti-k_{t} R=0.4, EM+GSC, |#eta_{jet}| < 0.8");
  else latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.77*yMax, "anti-k_{t} R=0.4, LC+GSC, |#eta_{jet}| < 0.8");
  ATLASLabel(0.3,0.84, "Preliminary");
*/
  if(isPhoton)  latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.85*yMax, "#sqrt{s}=13 TeV, DB with #gamma-jet");
  else latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.85*yMax, "#sqrt{s}=13 TeV, DB with Z-jet");
  if (isEM)latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.77*yMax, "anti-k_{t} R=0.4, EM+GSC, |#eta_{jet}| < 0.8");
  else if (isPFlow) latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.77*yMax, "anti-k_{t} R=0.4, PFlow, |#eta_{jet}| < 0.8");
  else latex->DrawLatex( xfrac*(xMax-xMin)+xMin, 0.77*yMax, "anti-k_{t} R=0.4, LC+GSC, |#eta_{jet}| < 0.8");
  ATLASLabel(0.28,0.84, "Preliminary");
  c1->SaveAs("Systs.C");
  c1->SaveAs("Systs.pdf");
  h_total->SaveAs("TotalSyst.root");
  
}

