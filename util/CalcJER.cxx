#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TFile.h>

#include "SystTool/SystTool.h"
#include "SystTool/SystContainer.h"

using namespace std;

vector<double> vectorize(string bins)
{
    istringstream stream(bins);
    string bin;
    
    vector<double> output;
    
    while (getline(stream, bin, ',')) {
        output.push_back( atof(bin.c_str()) );
    }
    
    return output;
}

vector<string> vectorizeStr(string bins)
{
    istringstream stream(bins);
    string bin;
    
    vector<string> output;
    
    while (getline(stream, bin, ',')) {
        while (bin[0] == ' ') bin.erase(bin.begin());
        output.push_back(bin);
    }
    
    return output;
}

/** Example program to get JER systematics */
int main(int argc, char *argv[])
{
    vector<string> jetAlgo = vectorizeStr("AntiKt4LCTopo");
    
    vector<string> systNames = vectorizeStr("nominal, NoSyst, dPhi__1up, dPhi__1down, Veto__1up, Veto__1down, JVT__1up, JVT__1down, EG_SCALE_ALL__1up, EG_SCALE_ALL__1down, EG_RESOLUTION_ALL__1up, EG_RESOLUTION_ALL__1down");
    vector<double> ptBins = vectorize("17., 20., 25., 30., 35., 45., 60., 80., 110., 160., 210., 260.");
   
    TFile *output = TFile::Open("syst_JER.root", "recreate");
    
    for (unsigned int iAlg = 0; iAlg < jetAlgo.size(); ++iAlg) {
        cout << jetAlgo.at(iAlg) << endl;
        SystContainer *systData = new SystContainer(systNames, ptBins, 100);
       // systData->readFromFile("syst_data_" + jetAlgo.at(iAlg) + ".root");
        systData->readFromFile("../user.ctosciri.merged_DATA_new","bal");

        SystContainer *systMCTruth = new SystContainer(systNames, ptBins, 100);
        //systMCTruth->readFromFile("syst_MC_" + jetAlgo.at(iAlg) + "_truth.root");
        systMCTruth->readFromFile("../user.ctosciri.merged_MC_new","bal");

        SystTool *sTool = new SystTool(systNames, ptBins);
        sTool->setSystData(systData);
        sTool->setSystMCTruth(systMCTruth);
        
        for (unsigned int iSyst = 1; iSyst < systNames.size(); ++iSyst) { //loop over systematics
            cout << systNames.at(iSyst) << endl;
            
            sTool->runToysJER(systNames.at(iSyst));

            TH1D* h_syst = sTool->getSystHist(jetAlgo.at(iAlg) + "_" + systNames.at(iSyst));

            output->cd();
            h_syst->Write();

            // Additional histograms
            
            // MC/Data systematics
            TH1D *h_systMC = sTool->getSystHistMC(jetAlgo.at(iAlg) + "_" + systNames.at(iSyst));
            TH1D *h_systData = sTool->getSystHistData(jetAlgo.at(iAlg) + "_" + systNames.at(iSyst));
            
            output->cd();
            //h_systMC->Write();
            //h_systData->Write();
            
            // Toys histograms
            vector<TH1D*>* toys = sTool->getToysHist(jetAlgo.at(iAlg) + "_" + systNames.at(iSyst));
            output->cd();
            for (unsigned int i = 0; i < toys->size(); ++i) {
                //toys->at(i)->Write();
            }

            vector<TH1D*>* toysMC = sTool->getToysHistMC(jetAlgo.at(iAlg) + "_" + systNames.at(iSyst));
            vector<TH1D*>* toysData = sTool->getToysHistData(jetAlgo.at(iAlg) + "_" + systNames.at(iSyst));

            output->cd();
            for (unsigned int i = 0; i < toysMC->size(); ++i) {
               // toysMC->at(i)->Write();
               // toysData->at(i)->Write();
            }
        }
        
        delete sTool;
        delete systData;
        delete systMCTruth;
    }
    
    output->Close();
    
    return 0;
}
