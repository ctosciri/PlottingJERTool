#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TFile.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include "TROOT.h"
#include <TLegend.h>


#include "PlottingTools/PlottingTools.h"


using namespace PlottingTools;

/** Example program to get JES systematics */
int main(int argc, char *argv[])
{
    SetAtlasStyle();


  int ip=1;
  char _namebuffer[100];
  std::string DataFileName = "syst_data_AntiKt4LCTopo.root"; 
  std::string MCFileName   = "syst_MC_AntiKt4LCTopo.root";
  string JetAlg        = "AntiKt4LCTopo";
  string _variable  = "RefPT";
  TString _outfile     = "syst_JES.root"; 

  while (ip<argc) {
    cout << "ip is " << ip << endl;
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          DataFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno Data file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--MC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          MCFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno MC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--jetAlg" or string(argv[ip])=="--alg") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          JetAlg=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--output") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outfile=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--variable") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _variable=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output specified"<<std::endl; break;}
      }
    }
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Comparing " << DataFileName << " to " << MCFileName << endl;
  cout << "Jet Alogorithm set to: " << JetAlg << endl;
  cout << "These settings can be changed using " << endl;
  cout << "doSyst --data MyDataFileName.root --mc MyMCFileName.root --alg JetAlgorithmUsed" << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
  
  TFile* tmpFile = TFile::Open(TString(DataFileName), "read");
  TList* list = tmpFile->GetListOfKeys();
  TIter next(list); 
  TObject* object = 0; 
  vector<string> systNames;

  
  TFile *output = TFile::Open(_outfile, "recreate");
    
  TFile* dataFile = TFile::Open(TString( TString(DataFileName))); //"syst_data_" + JetAlg + ".root"));
  TFile* MCFile   = TFile::Open(TString( TString(MCFileName))); //"syst_MC_" + JetAlg + ".root"));

  TCanvas* cComp = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
  cComp->Divide( 1, 2, 0.0, 0.01, 0 );
  //cComp->cd(1);
  //gPad -> SetLogy();
  //cComp->cd(2);
  //gPad -> SetLogx();

  sprintf( _namebuffer, "h%s", _variable.c_str());

  TH1D* Data = (TH1D*) dataFile->Get(_namebuffer);
  Data->SetMarkerStyle(20);
  Data->SetMarkerSize(1.0);
  Data->SetMarkerColor(1);
  
  TH1D* MC   = (TH1D*) MCFile->Get(_namebuffer);
  MC->SetMarkerStyle(20);
  MC->SetMarkerSize(1.0);
  MC->SetMarkerColor(2);


  //Data->Scale(1/Data->Integral());
  double scale;
  scale = Data->Integral()/MC->Integral();
//  scale = Data->Integral(37, 75)/MC->Integral(37, 75);
  //scale = 4.07329;
  MC->Scale(scale); //Data->Integral()/MC->Integral());
//  Data->Rebin(2);
//  MC->Rebin(2);
  Data->Print("all");
//  cout << scale << endl;


  TGraphErrors* gRatio = new TGraphErrors(); 
  gRatio->SetMarkerStyle(20);
  gRatio->SetMarkerSize(1.0);
  gRatio->SetMarkerColor(1);

//  PrepareTwoPadCanvas( cComp, "Ref #eta", "Entries", "Data/MC", -5, 5, 0.1, 200, 0.6001, 1.3999, 505); //0.9001, 1.09999, 505);
//  PrepareTwoPadCanvas( cComp, "P_{T}^{leading Jet}", "Entries", "Data/MC", 0, 500, 0.122, 500, 0.6001, 1.3999, 505);
//    PrepareTwoPadCanvas( cComp, "Ref Mass", "Entries", "Data/MC", 65, 115, 0.0, 600, 0.6001, 1.3999, 505);
    PrepareTwoPadCanvas( cComp, "#mu", "Entries", "Data/MC", 0.5, 100.5, 0.001, 20000, 0.6001, 1.3999, 505);


  for ( int n = 0; n < Data->GetNbinsX(); ++n ) {
    if (Data->GetBinContent(n) == 0 or MC->GetBinContent(n)==0) continue;
//    if (Data->GetY()[n] == -999 or MC->GetY()[n]==-999) continue;
    gRatio->SetPoint(n, 0.5*(Data->GetBinCenter(n) + MC->GetBinCenter(n)), Data->GetBinContent(n) / MC->GetBinContent(n));
    double yerr = Data->GetBinContent(n) / MC->GetBinContent(n) * sqrt( pow(Data->GetBinError(n)/Data->GetBinContent(n),2) + pow(MC->GetBinError(n)/MC->GetBinContent(n),2) );
    gRatio->SetPointError(n, Data->GetBinWidth(n)/2, yerr);
  }
 
  TLegend *leg = new TLegend(0.65,0.65,0.85,0.85,NULL,"brNDC");
  leg->SetBorderSize(0);
  leg->AddEntry(Data, "Data", "lp");
  leg->AddEntry(MC,    "MC", "lp");


  PlotConstantLine( 1.0 , -5, 1000, 2, 1 );
  PlotConstantLine( 0.90, -5, 1000, 3, 1 );
  PlotConstantLine( 1.10, -5, 1000, 3, 1 );

  cComp->cd(1);
  Data->Draw("sameP");
  MC->Draw("sameP");
  leg->Draw("same");
  cComp->cd(2);
  gRatio->Draw("sameP");
  
   
  sprintf( _namebuffer, "%s_Ratio.pdf", _variable.c_str());
  cComp->SaveAs(_namebuffer);
  output->cd();
  cComp->Write();
  output->Close();
  return 0;
}

