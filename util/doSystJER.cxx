#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TFile.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLegend.h>


#include "PlottingTools/PlottingTools.h"

using namespace PlottingTools;
using namespace std;

/** Example program to get JES systematics */
int main(int argc, char *argv[])
{


  SetAtlasStyle();
  int ip=1;
  int lastround=0;
  char _namebuffer[100];
  std::string DataFileName = "../Data_EM_4Dec";
  std::string MCFileName   = "../Sherpa_EM_4Dec";
  string altMCFileName     = "../Pythia_EM_4Dec";
  string JetAlg            = "AntiKt4EMTopo";
  string calibType	   = "Gammajet";
  string _measurement      = "bal";
  string _DataLabel        = "";
  string _ScaleLabel       = "";
  TString _outfile         = "syst_JER.root"; 

  while (ip<argc) {
    if (ip==lastround){cout << "check arguments " << lastround << endl; break;}
    lastround = ip;
    cout << "ip is " << ip << endl;
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          DataFileName=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno Data file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--MC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          MCFileName=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno MC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--altMC" or string(argv[ip])=="--AltMC") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          altMCFileName=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--jetAlg" or string(argv[ip])=="--alg") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          JetAlg=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--calibType") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          calibType=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--output") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outfile=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--measurement") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _measurement=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--DataLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _DataLabel=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--ScaleLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _ScaleLabel=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno output specified"<<std::endl; break;}
      }
    }
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Comparing " << DataFileName << " to " << MCFileName << endl;
  cout << "Jet Alogorithm set to: " << JetAlg << endl;
  cout << endl;
  cout << "These settings can be changed using " << endl;
  cout << "doSyst --data MyDataFileName.root --mc MyMCFileName.root --alg JetAlgorithmUsed" << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
 

  TFile* tmpFile = TFile::Open(TString(DataFileName), "read");
  TList* list = tmpFile->GetListOfKeys();
  TIter next(list); 
  TObject* object = 0; 
  vector<string> systNames;

  while ((object = next())) {
    if ( strstr((object->GetName()), "bootstrap_bal") == NULL) continue;
    else if (strstr((object->GetName()), "MET") != NULL) continue;
    else {
      string tmpFile(object->GetName());
      string tmp2 = tmpFile.erase(0, 14);
      cout << "printing content from data " << tmp2 << endl;
      systNames.push_back(tmp2);
    }
  }
  
  vector<double> ptBins;

  Int_t nBins=0;
  TH2D* hist2d = (TH2D*)tmpFile->Get( "hResponseProbePT" );
  for(int i=1;i<=hist2d->GetXaxis()->GetNbins()+1;++i) {
    ptBins.push_back(hist2d->GetXaxis()->GetBinLowEdge(i)); 
    std::cout << "Cecilia's addition, ptBins(" << i << ") is " << hist2d->GetXaxis()->GetBinLowEdge(i) << std::endl;
  }

  vector<double> mappedBins;

//  mappedBins.push_back(20.8);  //28 gamma, 20.8 Z
  if(calibType == "ZJet" or calibType == "Zjet")  mappedBins.push_back(17.);
  else mappedBins.push_back(28.);


  TGraphErrors* mapping = (TGraphErrors*)tmpFile->Get("MappingVsProbePT_graph");
  for ( int n = 0; n < mapping->GetN()-1; ++n ) {
    if (fabs(mapping->GetY()[n])<0.001) continue;
    if((calibType == "ZJet" or calibType == "Zjet") and n==0){
      mappedBins.push_back(24.073);  // The reco Pt Vs. ref Pt curve is too flat at low Pt to not do this. 
      cout << " " << endl;
    }
    else mappedBins.push_back(2*mapping->GetY()[n]-mappedBins[n]); 
    cout << n << " " << 2*mapping->GetY()[n]-mappedBins[n] << endl;
    nBins+=1;
  }

  for (vector<double>::const_iterator i = mappedBins.begin(); i != mappedBins.end(); ++i) cout << *i << ' '; 
  cout << endl;

   
  tmpFile->Close();
  delete tmpFile; 


  TFile *output_original = TFile::Open(_outfile, "recreate");
  TFile *output_rebinned = TFile::Open( "rebinned.root", "recreate");
  TFile *output_toys     = TFile::Open( "toys.root", "recreate");

  SystContainer *systData = new SystContainer(systNames, ptBins, 100);
  systData->readFromFile( DataFileName, _measurement); //"syst_data_" + JetAlg + ".root", _measurement );
      
  SystContainer *systMC = new SystContainer(systNames, ptBins, 100);
  systMC->readFromFile( MCFileName, _measurement); //"syst_MC_" + JetAlg + ".root", _measurement);
  
  SystTool *sTool = new SystTool(systNames, ptBins);
  sTool->setSystData(systData);
  //sTool->setSystMC(systMC);
  sTool->setSystMCTruth(systMC);
  TH1D* Stat = 0; 
  TH1D* Purity=0;
  TH1D* PurityMeas=0;
  TH1D* nominal = 0;
  TH1D* AltMC=0;
  for (unsigned int iSyst = 0; iSyst < systNames.size(); ++iSyst) { //loop over systematics

    cout << "[doSystJER] Working on systematic: " << systNames.at(iSyst) << endl;

    if (not(systNames.at(iSyst).find("nominal") == std::string::npos)) continue;
//    if (not(systNames.at(iSyst).find("NoSyst") == std::string::npos) ) continue;
    if (not(systNames.at(iSyst).find("MET") == std::string::npos) ) continue;
    sTool->runToysJER(systNames.at(iSyst));
         
    TH1D* h_syst = sTool->getSystHist(JetAlg + "_" + systNames.at(iSyst));
    TH1D* h_SResol = sTool->getSystHistSResol(JetAlg + "_" + systNames.at(iSyst));
    TH1D* h_NResol = sTool->getSystHistNResol(JetAlg + "_" + systNames.at(iSyst));

    h_syst->Draw();
    TH1D* h_JERsyst = (TH1D*) h_syst->Clone(("h_JERsyst_" + systNames.at(iSyst)).c_str());
    TH1D* h_syst_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));
    TH1D* h_systJER_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));
  
    h_JERsyst->Sumw2();

    std::cout << "[doSystJER] \t Just h_syst_mapped" << std::endl;
    for (int i = 1; i < h_JERsyst->GetNbinsX()+1; i++) {
      std::cout << "[doSystJER] \t h_JERsyst, bin " << i << ": " << h_JERsyst->GetBinContent(i) << std::endl;
    }

    // N + f
    h_JERsyst->Add(h_NResol);

    std::cout << "[doSystJER] \t N+f" << std::endl;
    for (int i = 1; i < h_JERsyst->GetNbinsX()+1; i++) {
      std::cout << "[doSystJER] \t h_JERsyst, bin " << i << ": " << h_JERsyst->GetBinContent(i) << std::endl;
    }

    // (sqrt(N + f) - sqrt(N)) / sqrt(N)
    for (int i = 0; i < h_JERsyst->GetNbinsX(); i++) { 

      double bin_content = sqrt(h_JERsyst->GetBinContent(i)) - sqrt(h_NResol->GetBinContent(i));

      if (sqrt(h_NResol->GetBinContent(i)) != 0)
        bin_content /= sqrt(h_NResol->GetBinContent(i));
      else
        bin_content = 0.;

      h_JERsyst->SetBinContent(i, abs(bin_content));

      if (std::isnan(bin_content) || std::isinf(bin_content)) {
        h_JERsyst->SetBinContent(i, 0.);
      }
    }
 
    std::cout << "[doSystJER] \t (sqrt(N + f) - sqrt(N)) / sqrt(N)" << std::endl;
    for (int i = 1; i < h_JERsyst->GetNbinsX()+1; i++) {
      std::cout << "[doSystJER] \t h_JERsyst, bin " << i << ": " << h_JERsyst->GetBinContent(i) << std::endl;
    }

    if (systNames.at(iSyst) == "J2__1down") {
      h_systJER_mapped->SetName(TString(calibType + "_" + "Veto__1down_"+JetAlg)); //systNames.at(iSyst)));
      h_systJER_mapped->SetTitle(TString(calibType + "_" + "Veto__1down_"+JetAlg));
    }
    else if (systNames.at(iSyst) == "J2__1up") {
      h_systJER_mapped->SetName(TString(calibType + "_" + "Veto__1up_"+JetAlg)); 
      h_systJER_mapped->SetTitle(TString(calibType + "_" + "Veto__1up_"+JetAlg));
    }
    else h_systJER_mapped->SetName(TString(calibType + "_" + systNames.at(iSyst)+"_"+JetAlg));
    //cout << systNames.at(iSyst) << endl;
          
    output_original->cd();
   
    int nbinsx = h_syst->GetNbinsX();
    for (int i = 0; i < nBins; ++i){
       h_syst_mapped->SetBinContent(i, h_syst->GetBinContent(i));h_syst_mapped->SetBinError(i, h_syst->GetBinError(i));
       h_systJER_mapped->SetBinContent(i, h_JERsyst->GetBinContent(i));h_systJER_mapped->SetBinError(i, h_JERsyst->GetBinError(i));
    }

    TH1D* tmp = SystUtils::rebinUntilSignificant( (*h_syst_mapped), 15, 2000, 2.);
    TH1D* tmp2 = SystUtils::rebinUntilSignificant( (*h_systJER_mapped), 15, 2000, 2.);

    //h_syst_mapped->Write();   
    h_systJER_mapped->Write();

    output_rebinned->cd();
    if (systNames.at(iSyst) == "J2__1down") {
      tmp2->SetName(TString(calibType + "_" + "Veto__1down_"+JetAlg)); //systNames.at(iSyst)));
      tmp2->SetTitle(TString(calibType + "_" + "Veto__1down_"+JetAlg));
    }
    else if (systNames.at(iSyst) == "J2__1up") {
      tmp2->SetName(TString(calibType + "_" + "Veto__1up_"+JetAlg));
      tmp2->SetTitle(TString(calibType + "_" + "Veto__1up_"+JetAlg));
    }
    else tmp2->SetName(TString(calibType + "_" + systNames.at(iSyst)+"_"+JetAlg));
    tmp2->SetTitle("rebinned");
    tmp2->Write();
    if (iSyst == 1) {Stat = tmp2;} //  cout << "in" << endl; Stat->Print("all"); }
 
    TH1D *h_systMC = sTool->getSystHistMC(JetAlg + "_" + systNames.at(iSyst));
    TH1D *h_systData = sTool->getSystHistData(JetAlg + "_" + systNames.at(iSyst));

    output_toys->cd();
    h_systMC->Write();
    h_systData->Write();

    ///////////////////////////////////////////////
    // Writing all of the toy distributions used to 
    // obtain the statistical uncertainty in a 
    // seperate output file
    ///////////////////////////////////////////////
    vector<TH1D*>* toys = sTool->getToysHist(JetAlg + "_" + systNames.at(iSyst));
    output_original->cd();
    for (unsigned int i = 0; i < toys->size(); ++i) {
      output_toys->cd();
      toys->at(i)->Write();
    }

    vector<TH1D*>* toysMC = sTool->getToysHistMC(JetAlg + "_" + systNames.at(iSyst));
    vector<TH1D*>* toysData = sTool->getToysHistData(JetAlg + "_" + systNames.at(iSyst));

    output_original->cd();
    for (unsigned int i = 0; i < toysMC->size(); ++i) {
      output_toys->cd();
      toysMC->at(i)->Write();
      toysData->at(i)->Write();
    }


  }

  TFile *f_JER = new TFile("/afs/cern.ch/user/c/ctosciri/private/PlottingTools/Output_Root/output_resolution.root");//File MC JER
  TFile *f_Closure = new TFile("/afs/cern.ch/user/c/ctosciri/private/PlottingTools/Output_Root/RatioRecoTruth.root");//File Reco-Truth Ratio resolution
  cout<<"Open files"<<endl;

  //TH1D* JER_Pythia = (TH1D*)f_JER->Get("JER_btag77_Pythia");
  //TH1D* JER_Sherpa = (TH1D*)f_JER->Get("JER_btag77_Sherpa");
  
  //delete these 2 lines and ripristinate the previous 2

  TH1D* JER_Pythia = (TH1D*)f_JER->Get("JER_btag77_Sherpa");
  TH1D* JER_Sherpa = (TH1D*)f_JER->Get("JER_btag77_Pythia");

  TH1D* RatioRecoTruth = (TH1D*)f_Closure->Get("RecoTruthRatio_btag77_Sherpa");
  TH1D* JER_Pythia_reco = (TH1D*)f_JER->Get("JER_btag77_Pythiawith_Reco_jet");

  TH1D* JER_Pythia_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));
  TH1D* JER_Sherpa_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));
  TH1D* RatioRecoTruth_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));
  TH1D* JER_Pythia_reco_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));

  //JER_Pythia->Sumw2();
  //JER_Sherpa->Sumw2();
  //RatioRecoTruth->Sumw2();

  TH1D* h_MCdifference_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));
  TH1D* h_Closure_mapped = new TH1D("", "",nBins, (&(*(mappedBins.begin()))));

  for (int i = 0; i < nBins; ++i){
     JER_Pythia_mapped->SetBinContent(i, (JER_Pythia->GetBinContent(i)));
     JER_Pythia_mapped->SetBinError(i, (JER_Pythia->GetBinError(i)));

     JER_Sherpa_mapped->SetBinContent(i, (JER_Sherpa->GetBinContent(i)));
     JER_Sherpa_mapped->SetBinError(i, (JER_Sherpa->GetBinError(i)));

     RatioRecoTruth_mapped->SetBinContent(i, (RatioRecoTruth->GetBinContent(i)));
     RatioRecoTruth_mapped->SetBinError(i, (RatioRecoTruth->GetBinError(i)));

     JER_Pythia_reco_mapped->SetBinContent(i, (JER_Pythia_reco->GetBinContent(i)));
     JER_Pythia_reco_mapped->SetBinError(i, (JER_Pythia_reco->GetBinError(i)));

     if(JER_Pythia->GetBinContent(i)>0 && JER_Sherpa->GetBinContent(i)>0){
      h_MCdifference_mapped->SetBinContent(i, abs((JER_Pythia->GetBinContent(i)/JER_Sherpa->GetBinContent(i))-1));
      cout<<"::::::::::::::::::::::::::::::::::::::::::::::::::::"<<endl;
      cout<<"JER Pythia  "<< JER_Pythia->GetBinContent(i)<< endl;
      cout<<"JER Sherpa  "<< JER_Sherpa->GetBinContent(i)<< endl;      
      cout<<"JER Pythia/JER Sherpa  "<<((JER_Pythia->GetBinContent(i)/JER_Sherpa->GetBinContent(i))-1)<< endl;
      cout<<"::::::::::::::::::::::::::::::::::::::::::::::::::::"<<endl;
     //h_MCdifference_mapped->SetBinError(i, (JER_Pythia->GetBinError(i)/JER_Sherpa->GetBinError(i)));
     }
     else {h_MCdifference_mapped->SetBinContent(i, 0.);}
     if(JER_Pythia_reco->GetBinContent(i)!=0 && RatioRecoTruth->GetBinContent(i)!=0){
       h_Closure_mapped->SetBinContent(i, abs((RatioRecoTruth->GetBinContent(i)/JER_Pythia_reco->GetBinContent(i))-1));
     //h_Closure_mapped->SetBinError(i, (RatioRecoTruth->GetBinError(i)/JER_Pythia->GetBinError(i)));
     }
     else h_Closure_mapped->SetBinContent(i, 0.);
  }

  output_original->cd();
  h_MCdifference_mapped->SetName("MCdifference");
  h_MCdifference_mapped->Write();
  h_Closure_mapped->SetName("Closure");
  h_Closure_mapped->Write();

  TCanvas* Compare = new TCanvas( "JER", "JER", 200, 10, 700, 780 );
  //Compare->Divide( 1, 2, 0.0, 0.01, 0 );
  Compare->cd();
  gPad -> SetLogx();
  //cComp->cd(2);
  //gPad -> SetLogx();
  //JER_Pythia_mapped->Write(); 
  //JER_Sherpa_mapped->Write("same");

  TFile* data = TFile::Open(TString( TString(DataFileName))); //"syst_data_" + JetAlg + ".root"));
  TFile* MC   = TFile::Open(TString( TString(MCFileName))); //"syst_MC_" + JetAlg + ".root"));
  TFile* AltMCFile = TFile::Open(TString(altMCFileName));

  TCanvas* cComp = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
  cComp->Divide( 1, 2, 0.0, 0.01, 0 );
  cComp->cd(1);
  gPad -> SetLogx();
  cComp->cd(2);
  gPad -> SetLogx();

  double xMax = 1600;
  double xMin = 10;
  //double yMax = 1.19001;
  double yMax = 1.3;
  //double yMin = 0.35001;
  double yMin = 0.5;

  if (_measurement == "mpf") PrepareTwoPadCanvas( cComp, "P_{T}^{JES}", "R_{DB}", "Ratio w.r.t Pythia", xMin, xMax, yMin, yMax, 0.8001, 1.199, 505);
  else if (_measurement == "bal") PrepareTwoPadCanvas( cComp, "P_{T}^{JES}", "p_{T}^{jet} / p_{T}^{ref}", "Ratio w.r.t Pythia", xMin, xMax, yMin, yMax, 0.8001, 1.199, 505);


  TGraphErrors* DataGraph = (TGraphErrors*) data->Get("ResponseVsProbePT_graph");
  DataGraph->SetMarkerStyle(20);
  DataGraph->SetMarkerSize(1.0);
  DataGraph->SetMarkerColor(1);

  TGraphErrors* MCGraph   = (TGraphErrors*) MC->Get("ResponseVsProbePT_graph");
  MCGraph->SetMarkerStyle(20);
  MCGraph->SetMarkerSize(1.0);
  MCGraph->SetMarkerColor(2);

  TGraphErrors* gRatio = new TGraphErrors(); 
  gRatio->SetMarkerStyle(20);
  gRatio->SetMarkerSize(1.0);
  gRatio->SetMarkerColor(1);

  Stat->Reset();
  Stat->SetTitle(TString(calibType + "_Stat_"+JetAlg));
  Stat->SetName(TString(calibType + "_Stat_"+JetAlg));

  PurityMeas = (TH1D*)data->Get("PurityHist");
  Purity=(TH1D*)Stat->Clone();
  Purity->SetTitle(TString(calibType + "_Purity_"+JetAlg));
  Purity->SetName(TString(calibType + "_Purity_"+JetAlg));


  nominal = (TH1D*)Stat->Clone();
  nominal->SetTitle(TString(calibType + "_Nominal_"+JetAlg));
  nominal->SetName(TString(calibType + "_Nominal_"+JetAlg));

  TGraphErrors* AltMCGraph   = (TGraphErrors*) AltMCFile->Get("ResponseVsProbePT_graph");
  AltMCGraph->SetMarkerStyle(20);
  AltMCGraph->SetMarkerSize(1.0);
  AltMCGraph->SetMarkerColor(4);
 
  TGraphErrors* MCRatio = new TGraphErrors();
  MCRatio->SetMarkerStyle(20);
  MCRatio->SetMarkerSize(1.0);
  MCRatio->SetMarkerColor(4);

  AltMC=(TH1D*)Stat->Clone();
  AltMC->SetTitle(TString(calibType + "_MC_"+JetAlg));
  AltMC->SetName(TString(calibType + "_MC_"+JetAlg));

  TString channel="#gamma";
  TString nomMC="Pythia";
  if(calibType == "ZJet" or calibType == "Zjet") {channel="Z"; nomMC="Powheg+Pythia";}


  for ( int n = 0; n < DataGraph->GetN()-1; ++n ) {
    if (DataGraph->GetY()[n] == 0 or MCGraph->GetY()[n]==0) continue;
    if (DataGraph->GetY()[n] == -999 or MCGraph->GetY()[n]==-999) continue;
    gRatio->SetPoint(n, 0.5*(mappedBins[n+1] + mappedBins[n]), DataGraph->GetY()[n] / MCGraph->GetY()[n]);
//    gRatio->SetPoint(n, 0.5*(DataGraph->GetX()[n] + MCGraph->GetX()[n]), DataGraph->GetY()[n] / MCGraph->GetY()[n]);
    double yerr = DataGraph->GetY()[n] / MCGraph->GetY()[n] * sqrt( pow(DataGraph->GetEY()[n]/DataGraph->GetY()[n],2) + pow(MCGraph->GetEY()[n]/MCGraph->GetY()[n],2) );
    gRatio->SetPointError(n, 0.5*(mappedBins[n+1] - mappedBins[n]), yerr);
//    gRatio->SetPointError(n, DataGraph->GetEX()[n], yerr);
    Stat->SetBinContent(n+1, yerr);

    //if (channel=="#gamma") Purity->SetBinContent(n+1,PurityMeas->GetBinContent(n+1)); 

    nominal->SetBinContent(n+1, DataGraph->GetY()[n] / MCGraph->GetY()[n]);
      if(AltMCGraph->GetY()[n] == 0 or AltMCGraph->GetY()[n] == -999)continue;
//      MCRatio->SetPoint(n, 0.5*(AltMCGraph->GetX()[n] + MCGraph->GetX()[n]), AltMCGraph->GetY()[n] / MCGraph->GetY()[n]);
      MCRatio->SetPoint(n, 0.5*(mappedBins[n+1] + mappedBins[n]), AltMCGraph->GetY()[n] / MCGraph->GetY()[n]);
      yerr = AltMCGraph->GetY()[n] / MCGraph->GetY()[n] * sqrt( pow(AltMCGraph->GetEY()[n]/AltMCGraph->GetY()[n],2) + pow(MCGraph->GetEY()[n]/MCGraph->GetY()[n],2) );
      MCRatio->SetPointError(n, 0.5*(mappedBins[n+1] - mappedBins[n]), yerr);
      AltMC->SetBinContent(n+1, 1-(AltMCGraph->GetY()[n] / MCGraph->GetY()[n]));
      AltMC->SetBinError(n+1, yerr);
      //cout << MCGraph->GetY()[n] << endl;
    

     
     AltMCGraph->SetPoint(n, 0.5*(mappedBins[n+1] + mappedBins[n]), AltMCGraph->GetY()[n]); 
     AltMCGraph->SetPointError(n, 0.5*(mappedBins[n+1] - mappedBins[n]), AltMCGraph->GetEY()[n]);
     MCGraph->SetPoint(n, 0.5*(mappedBins[n+1] + mappedBins[n]), MCGraph->GetY()[n]);
     MCGraph->SetPointError(n, 0.5*(mappedBins[n+1] - mappedBins[n]), MCGraph->GetEY()[n]);
     DataGraph->SetPoint(n, 0.5*(mappedBins[n+1] + mappedBins[n]), DataGraph->GetY()[n]);
     DataGraph->SetPointError(n, 0.5*(mappedBins[n+1] - mappedBins[n]), DataGraph->GetEY()[n]);
    }


    // First point for Zjet at LC
//  AltMC->SetBinContent(1, -0.00964837); //-0.0293662);
//  AltMC->SetBinError(1, 0.00779331); //0.00953626);
   
  TLegend *leg = new TLegend(0.6,0.1,0.8,0.3,NULL,"brNDC");
  leg->SetBorderSize(0);

  leg->AddEntry(DataGraph, "Data", "lp");
  leg->AddEntry(MCGraph,   nomMC, "lp");
//  leg->AddEntry(MCGraph,   "Pythia", "lp");
  leg->AddEntry(AltMCGraph,   "Sherpa", "lp");

  TLatex *label = new TLatex(); 
  label->SetTextColor(1);
  label->SetTextFont(42);
  label->SetTextSize(0.04);


  PlotConstantLine( 1.0 , xMin, xMax, 2, 1 );
  PlotConstantLine( 0.95, xMin, xMax, 3, 1 );
  PlotConstantLine( 1.05, xMin, xMax, 3, 1 );

  cComp->cd(1);
//  MCRatio->SetPoint(0, 29.955, 1.17116);
//  MCRatio->SetPointError(0, 1.95499,0.00953626);

//  Lowest bin in for sherpa in gamma+jet doesn't make sense, don't show it
//  AltMCGraph->RemovePoint(0);

  AltMCGraph->Draw("sameP");
  DataGraph->Draw("sameP");
  MCGraph->Draw("sameP");

  leg->Draw("same");
  label->DrawLatex( 19, 1.1, _DataLabel.c_str());
  label->DrawLatex( 19, 1.02, _ScaleLabel.c_str());
  TLatex *latex = new TLatex();
  latex->SetTextColor(1);
  latex->SetTextFont(42);
  latex->SetTextSize(0.04);

  cComp->cd(1);

  TString scale ="EM";
  if (JetAlg=="AntiKt4EMTopo") scale = "EM";
  if (JetAlg=="AntiKt4EMPFlow") scale = "PFlow";
/*  TString channel="#gamma";
  TString nomMC="Pythia";
  if(calibType == "ZJet" or calibType == "Zjet") {channel="Z"; nomMC="Powheg+Pythia";}
  */
  ATLASLabel(0.27,0.83, "Preliminary", true);
  latex->DrawLatex( 0.01*(xMax-xMin)+xMin, 0.85*(yMax-yMin)+yMin, "#sqrt{s}=13 TeV, DB with "+channel+"-jet");
  if (JetAlg=="AntiKt4EMPFlow") latex->DrawLatex( 0.01*(xMax-xMin)+xMin, 0.77*(yMax-yMin)+yMin, "WP77 b-jet anti-k_{t} R=0.4, "+scale+", |#eta_{jet}| < 0.8");
  else latex->DrawLatex( 0.01*(xMax-xMin)+xMin, 0.77*(yMax-yMin)+yMin, "anti-k_{t} R=0.4, "+scale+"+GSC, |#eta_{jet}| < 0.8");

  cComp->cd(2);
  gRatio->Draw("sameP");
  gRatio->SetName("Ratio");
  gRatio->SaveAs("CentralValue.root");

//  Lowest bin in for sherpa in gamma+jet doesn't make sense, don't show it
  //MCRatio->RemovePoint(0);

  MCRatio->Draw("sameP");
   
  sprintf( _namebuffer, "%s_Ratio.pdf", _measurement.c_str());
  cComp->SaveAs(_namebuffer);
  sprintf( _namebuffer, "%s_Ratio.C", _measurement.c_str());
  cComp->SaveAs(_namebuffer);

  output_original->cd();
  Stat->Write();

  //if(doAltMC) 
  AltMC->Write();
  nominal->Write();
 
  if (channel=="#gamma") Purity->Write();
  output_rebinned->cd();

  Stat->Write();
  TH1D* tmp2 = SystUtils::rebinUntilSignificant( (*AltMC), 15, 600, 2.);


  tmp2->Write();
  nominal->Write();
  if (channel=="#gamma") Purity->Write();
  output_original->Close();
  return 0;
}

