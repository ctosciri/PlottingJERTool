#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include "PlottingTools/PlottingTools.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TFrame.h>
#include "TROOT.h"
#include <TLegend.h>

using namespace std;
using namespace PlottingTools;

void DrawHisto(TH1F *h, TString ytit, double min, double max) {
  h->GetYaxis()->SetRangeUser(min,max); h->SetXTitle("#it{p}_{T}^{ref} [GeV]"); h->SetYTitle(ytit);
  h->SetStats(0); h->Draw();
}

int main(int argc, char **argv) {
  SetAtlasStyle();
  //gROOT->LoadMacro("/home/ajhorton/atlasstyle-00-03-04/AtlasStyle.C");
  //gROOT->ProcessLine("SetAtlasStyle();");

  char _namebuffer[100];
  vector<double> VarBins;
  vector<double> RefPtBins;

  TString _histname = "hBalanceProbePT_Not_btag85";
  TString _datafilename = "";
  TString _mcfilename = "";
  TString _variable = "leadingJetEta";
  string _scaleLabel = "";
  double subleadingJetEtaMax=999., leadingJetEtaMax=999.;
 
  int ip=1;
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--histname") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _histname=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _datafilename=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--mc" or string(argv[ip])=="--MC") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _mcfilename=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--variable") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _variable=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno variable specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--leadingJetEtaMax") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          leadingJetEtaMax=atof(argv[ip+1]);
          ip+=2;
        } else {cout<<"\nno maximum for the subleading jet cut specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--subleadingJetEtaMax") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          subleadingJetEtaMax=atof(argv[ip+1]);
          ip+=2;
        } else {cout<<"\nno maximum for the delta phi cut specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--scaleLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _scaleLabel=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno scale name specified"<<std::endl; break;}
      }
    }
    /////////////////////////////////////////////////
    // If no options specified assume there's only
    // one argument and that argument is the filename
    /////////////////////////////////////////////////
    else {
      cout << argv[ip] << endl;
      cout << "Arguments must be specified using --file or --histname" << endl;
      cout << "Since this was not done assuming the first argument given is the filename" << endl;
      _datafilename=argv[ip];
      ip=argc;
    }
  }

  // ////////////////////////////////////////////////
  // Vectors to store the fitted and arithmatic means
  // of the response distributions for each Pt bin
  // ////////////////////////////////////////////////

  char prefix[40];
  if (_histname.Contains("Response"))  sprintf( prefix, "Response");
  else if (_histname.Contains("Balance")) sprintf( prefix, "Balance");


  char variable[40];
  sprintf(variable, _variable);

  char addCuts[100];
  if (leadingJetEtaMax<999)   sprintf(addCuts, "_MaxleadingJetEta_%1.2f", leadingJetEtaMax);
  else if (subleadingJetEtaMax<999) sprintf(addCuts, "_MaxsubleadingJetEta_%1.2f", subleadingJetEtaMax);
  else sprintf(addCuts, "");


  gErrorIgnoreLevel=2000; // removes Canvas print statements
  TFile *_datafile = TFile::Open(_datafilename, "UPDATE");
  TFile *_mcfile = TFile::Open(_mcfilename, "UPDATE");

  TH2D* ResponseHist = (TH2D*)_datafile->Get("hResponseProbePT");
  for(int i=1;i<=ResponseHist->GetXaxis()->GetNbins()+1;++i) {
    RefPtBins.push_back(ResponseHist->GetXaxis()->GetBinLowEdge(i));
  }

//  char namebuffer[30] = "";
  for(vector<int>::size_type i = 0; i != (RefPtBins.size()-1); i++) {
    vector<double> _mpfmean;
    vector<double> _mpfmeanError;
    vector<double> _mpfgausfit;
    vector<double> _mpfgausfitError;

    // /////////////////////////////////////////////////
    // Canvas which will hold the response distributions
    // for each set of radiation cuts
    // /////////////////////////////////////////////////
    sprintf( _namebuffer, "%s_distributions_in_%s_bins_%2.0f-%2.0f_data", prefix, variable, RefPtBins[i], RefPtBins[i+1]);
    TCanvas* binnedRespVsVarCan_data = new TCanvas( _namebuffer, _namebuffer );
    sprintf( _namebuffer, "%s_distributions_in_%s_bins_%2.0f-%2.0f_mc", prefix, variable, RefPtBins[i], RefPtBins[i+1]);
    TCanvas* binnedRespVsVarCan_mc   = new TCanvas( _namebuffer, _namebuffer );


    // //////////////////////////////////////////////////
    // Canvas and graph which will display response Vs Pt
    // //////////////////////////////////////////////////
    sprintf( _namebuffer, "%s_vs_%s_%2.0f-%2.0f", prefix, variable, RefPtBins[i], RefPtBins[i+1]);
    TCanvas* RespVsVarCan = new TCanvas( _namebuffer, _namebuffer );

    TCanvas* cComp = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
    cComp->Divide( 1, 2, 0.0, 0.01, 0 );

    float xmin=0, xmax=0;
    if (_variable == "leadingJetEta"){
      xmin = -5.;
      xmax = 5;
    } else if (_variable == "subleadingJetEta") {
      xmin = -6;
      xmax = 5;
    }
    float ymin = 0.55001; //0.35001
    float ymax = 1.39001; //1.19001
//    float ymin=0.35001;
//    float ymax=1.19001;
    TString xTitle;
    if (_variable == "leadingJetEta") xTitle= "#eta^{leading jet}";
    if (_variable == "subleadingJetEta")  xTitle= "#eta^{subleading jet}";

    if (_histname.Contains("Response"))  PrepareTwoPadCanvas( cComp, xTitle, "R_{MPF}", "Data/MC", xmin, xmax, ymin, ymax, 0.8001, 1.199, 505);

    TGraphErrors* RespVsVar_data = new TGraphErrors();
    sprintf( _namebuffer, "%sVs%s_%2.0f-%2.0f_data_graph", prefix, variable, RefPtBins[i], RefPtBins[i+1] );
    RespVsVar_data->SetName(_namebuffer);
    RespVsVar_data->SetTitle(_namebuffer);

    TGraphErrors* RespVsVar_mc = new TGraphErrors();
    sprintf( _namebuffer, "%sVs%s_%2.0f-%2.0f_mc_graph", prefix, variable, RefPtBins[i], RefPtBins[i+1] );
    RespVsVar_mc->SetName(_namebuffer);
    RespVsVar_mc->SetTitle(_namebuffer);


    sprintf(_namebuffer, "ResponseVsJetEta_%2.0f_to_%2.0f_GeV", RefPtBins[i], RefPtBins[i+1]); 
    TH3D* hist3d_data = (TH3D*)_datafile->Get( _namebuffer);
    if (leadingJetEtaMax<999)   hist3d_data->GetYaxis()->SetRange(0,hist3d_data->GetYaxis()->FindBin(leadingJetEtaMax)-1);
    if (subleadingJetEtaMax<999) hist3d_data->GetZaxis()->SetRange(hist3d_data->GetZaxis()->FindBin(subleadingJetEtaMax), hist3d_data->GetZaxis()->GetLast());
    sprintf(_namebuffer, "ResponseVsJetEta_%2.0f_to_%2.0f_GeV_data", RefPtBins[i], RefPtBins[i+1]);
    hist3d_data->SetName(_namebuffer);

    sprintf(_namebuffer, "ResponseVsJetEta_%2.0f_to_%2.0f_GeV", RefPtBins[i], RefPtBins[i+1]);
    TH3D* hist3d_mc = (TH3D*)_mcfile->Get( _namebuffer);
    if (leadingJetEtaMax<999)   hist3d_mc->GetYaxis()->SetRange(0,hist3d_mc->GetYaxis()->FindBin(leadingJetEtaMax)-1);
    if (subleadingJetEtaMax<999) hist3d_mc->GetZaxis()->SetRange(hist3d_mc->GetZaxis()->FindBin(subleadingJetEtaMax), hist3d_mc->GetZaxis()->GetLast());



    TH2D* hist2d_data=0;
    TH2D* hist2d_mc=0;
    if (_variable == "leadingJetEta") {
      hist2d_data = (TH2D*)hist3d_data->Project3D("xy");
      hist2d_mc   = (TH2D*)hist3d_mc  ->Project3D("xy");
    }
    else if (_variable == "subleadingJetEta") {
      hist2d_data = (TH2D*)hist3d_data->Project3D("xz");
      hist2d_mc   = (TH2D*)hist3d_mc  ->Project3D("xz");
    }
   hist2d_data->RebinX(2);
   hist2d_mc->RebinX(2);



    ResponseVs( hist2d_data, binnedRespVsVarCan_data, RespVsVar_data, prefix, variable);
    sprintf( _namebuffer, "%sVs%s_bins_%2.0f-%2.0f%s_data.pdf", prefix, variable, RefPtBins[i], RefPtBins[i+1], addCuts);
    binnedRespVsVarCan_data->SaveAs(_namebuffer);


    ResponseVs( hist2d_mc,   binnedRespVsVarCan_mc, RespVsVar_mc,   prefix, variable);
    sprintf( _namebuffer, "%sVs%s_bins_%2.0f-%2.0f%s_mc.pdf", prefix, variable, RefPtBins[i], RefPtBins[i+1], addCuts);
    binnedRespVsVarCan_mc->SaveAs(_namebuffer);

//    RespVsVarCan->cd();


  // Just here temporarily, litterally just ripped out of Jiri's old plotting macro
    RespVsVar_data->SetMarkerStyle(20);
    RespVsVar_data->SetMarkerSize(1.0);
    RespVsVar_data->SetMarkerColor(1);
    RespVsVar_data->SetLineWidth(1);
    RespVsVar_data->SetLineColor(1);

    RespVsVar_mc->SetMarkerStyle(21);
    RespVsVar_mc->SetMarkerSize(1.0);
    RespVsVar_mc->SetMarkerColor(2);
    RespVsVar_mc->SetLineWidth(1);
    RespVsVar_mc->SetLineColor(2);

    cComp->cd(1);
    TLegend *leg = new TLegend(0.2,0.65,0.4,0.8,NULL,"brNDC");
    leg->SetBorderSize(0);
    leg->AddEntry(RespVsVar_data, "Data", "lp");
    leg->AddEntry(RespVsVar_mc,   "Powheg+Pythia", "lp");

    RespVsVar_data->Draw("sameP");
    RespVsVar_mc->Draw("sameP");
    leg->Draw("same");


    double yCorLatex = 0.9;
    double xCorLatex = 0.4;
    double yDrop = 0.09;

    sprintf( _namebuffer, "#sqrt{s}=13 TeV, MPF with #gamma-jet");
    PrintLatex(_namebuffer, xCorLatex, yCorLatex, xmax, xmin, ymax, ymin);
    yCorLatex-=yDrop;
    sprintf( _namebuffer, "anti-k_{t} R=0.4, LC+GSC, |#eta_{jet}| < 0.8");
    if (_variable == "leadingJetEta") sprintf( _namebuffer, "anti-k_{t} R=0.4, LC+GSC");
    PrintLatex(_namebuffer, xCorLatex, yCorLatex, xmax, xmin, ymax, ymin);
    yCorLatex-=yDrop;


    sprintf( _namebuffer, "%2.0f < p_{T}^{Ref} < %2.0f GeV", RefPtBins[i], RefPtBins[i+1]);
    PrintLatex(_namebuffer, xCorLatex, yCorLatex, xmax, xmin, ymax, ymin);
    yCorLatex-=yDrop;

    if(subleadingJetEtaMax<999){
      sprintf( _namebuffer, "#Delta#phi_{Ref,Jet} > %2.2f", subleadingJetEtaMax);
      PrintLatex(_namebuffer, xCorLatex, yCorLatex, xmax, xmin, ymax, ymin);
      yCorLatex-=yDrop;
    }
    
    if(leadingJetEtaMax<999){
      sprintf( _namebuffer, "p_{T}^{leadingJetEta}/p_{T}^{Ref} < %2.2f", leadingJetEtaMax);
      PrintLatex(_namebuffer, xCorLatex, yCorLatex, xmax, xmin, ymax, ymin);
      yCorLatex-=yDrop;
    }

    sprintf( _namebuffer, _scaleLabel.c_str());
    PrintLatex(_namebuffer, xCorLatex, yCorLatex, xmax, xmin, ymax, ymin);
    yCorLatex-=yDrop;

    TGraphErrors* gRatio = new TGraphErrors();
    gRatio->SetMarkerStyle(20);
    gRatio->SetMarkerSize(1.0);
    gRatio->SetMarkerColor(1);
    for ( int n = 0; n < RespVsVar_data->GetN(); ++n ) {
      if (RespVsVar_data->GetY()[n] == 0 or RespVsVar_mc->GetY()[n]==0) continue;
      if (RespVsVar_data->GetY()[n] == -999 or RespVsVar_mc->GetY()[n]==-999) continue;
      gRatio->SetPoint(n, 0.5*(RespVsVar_data->GetX()[n] + RespVsVar_mc->GetX()[n]), RespVsVar_data->GetY()[n] / RespVsVar_mc->GetY()[n]);
      double yerr = RespVsVar_data->GetY()[n] / RespVsVar_mc->GetY()[n] * sqrt( pow(RespVsVar_data->GetEY()[n]/RespVsVar_data->GetY()[n],2) + pow(RespVsVar_mc->GetEY()[n]/RespVsVar_mc->GetY()[n],2) );
      gRatio->SetPointError(n, RespVsVar_data->GetEX()[n], yerr);
      //Stat->SetBinContent(n+1, yerr);
      //nominal->SetBinContent(n+1, RespVsVar_data->GetY()[n] / RespVsVar_mc->GetY()[n]);
    }
    cComp->cd(2);

    gRatio->Draw("sameP");
    PlotConstantLine( 1.0 , xmin, xmax, 2, 1 );
    PlotConstantLine( 0.95, xmin, xmax, 3, 1 );
    PlotConstantLine( 1.05, xmin, xmax, 3, 1 );


    sprintf( _namebuffer, "%sVs%s_%2.0f-%2.0f%s.pdf", prefix, variable, RefPtBins[i], RefPtBins[i+1], addCuts);
    cComp->SaveAs(_namebuffer);
//    RespVsVarCan->SaveAs(_namebuffer);
  }


}
