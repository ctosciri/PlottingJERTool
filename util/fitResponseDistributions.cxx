#include "../../JES_ResponseFitter/JES_ResponseFitter/JES_BalanceFitter.h"
#include <TFile.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TObjString.h>
#include "TEnv.h"
#include <sstream>

void DrawHisto(TH1D * h, TString ytit, double min, double max) {
  h->GetYaxis()->SetRangeUser(min,max); h->SetXTitle("#it{p}_{T}^{ref} [GeV]"); h->SetYTitle(ytit);
  h->SetStats(0); h->Draw();
}

int main(int argc, char **argv) {


  // Start reading input configuration
  TString inputFileName = "";
  TString outputFileName = "";
  TString toFitCode = "PtTruth";
  TString pdfTag = "Default";
  bool doGaussian = false;
  int ip=1;
  while (ip<argc) {

	if (std::string(argv[ip]).substr(0,2)=="--") {

      // Get input file to run on
      if (std::string(argv[ip])=="--infile") {
        if (ip+1<argc && std::string(argv[ip+1]).substr(0,2)!="--") {
          inputFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno input file specified"<<std::endl; break;}
      }

      // Get name for output file to create
      else if (std::string(argv[ip])=="--outfile") {
        if (ip+1<argc && std::string(argv[ip+1]).substr(0,2)!="--") {
          outputFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output file specified"<<std::endl; break;}
      }

      // Format of files to fit
      else if (std::string(argv[ip]) == "--toFit") {
        if (ip+1<argc) {
          toFitCode=argv[ip+1];
          ip+=2;
          std::cout << "Hists to fit begin with: " << toFitCode << std::endl;
        } else {std::cout<<"\nno hist pattern to fit specified"<<std::endl; break;}
      }
      
      // Format of files to fit
      else if (std::string(argv[ip]) == "--pdfTag") {
        if (ip+1<argc) {
          pdfTag=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno pdf tag specified"<<std::endl; break;}
      }

      //use resolution width
      else if (std::string(argv[ip])=="--doGaussianFit") {
        doGaussian = true;
        ip+=1;
      }

      //unknown command
      else {
        std::cout<<"\nsetLimitsOneMassPoint: command '"<< std::string(argv[ip])<<"' unknown"<<std::endl;
        if (ip+1<argc && std::string(argv[ip+1]).substr(0,2)!="--") ip+=2;
        else ip+=1;
      }
    }//end if "--command"

	else { //if command does not start with "--"
      std::cout << "\nsetLimitsOneMassPoint: command '"<< std::string(argv[ip])<<"' unknown"<<std::endl;
      break;
    }//end if "--"

  }//end while loop

  gErrorIgnoreLevel=2000; // removes Canvas print statements
  double NsigmaForFit = 1.6;

  TFile *f = TFile::Open(inputFileName);
  
  JES_BalanceFitter *myFitter = new JES_BalanceFitter(NsigmaForFit);
  
  TString fitDesc, pdf, jetDesc="Anti k_{t} #it{R} = 0.4, EM+JES";
  if (doGaussian) {
    pdf=Form("fitplots/Response_Gauss_fits_%s.pdf",pdfTag.Data());
    fitDesc = "Gaussian fit";
  } else {
    myFitter->SetPoisson();
    pdf=Form("fitplots/Response_Poisson_fits_%s.pdf",pdfTag.Data());
    fitDesc="Modified Poisson fit";
  }

  // Get list of all histograms in this file which match our pattern.
  std::cout << "Getting list of histogram names" << std::endl;
  std::vector<TString> histNamesToUse;
  f->cd();
  TList * theseHistKeys = gDirectory->GetListOfKeys();
  TIter next(theseHistKeys);
  while (TObject *obj = next()) {
    TString thisname = obj->GetName();
    if (thisname.Contains(toFitCode)) { 
      histNamesToUse.push_back(thisname);
      std::cout << "Adding name " << thisname << std::endl;
    }
  }

  // Want one histogram each of JES and JER per eta and npv bin, binned in pt
  std::vector<double> pTBins;
  std::vector<double> etaBins;
  std::vector<double> NPVBins;

  std::vector<std::vector< double > > results;

  TCanvas *can = new TCanvas();
  can->SetMargin(0.12,0.04,0.12,0.04);

  can->Print(pdf+"[");
  for (unsigned int i=0; i< histNamesToUse.size(); i++) {
  
    TString inHistName = histNamesToUse.at(i);
    std::cout << "Fitting histogram " << inHistName << std::endl;

    double minPt = inHistName.Contains("TopoEM") ? 13.0 : 10.0;

    // Extract low end of pT bin from name to use in fit.
    TObjArray *stringComponents = inHistName.Tokenize("_");
    TString ptlowstr;
    double etalow, etahigh, ptlow, pthigh, NPVlow, NPVhigh;
    TString storeLastString = "";
    int count = 0;
    for (Int_t token = 0; token < stringComponents->GetEntries(); token++) {
      TString thistoken = ((TObjString *)(stringComponents->At(token)))->String();
      std::stringstream ss(thistoken.Data());
      if (!(storeLastString == "")) {
        if (storeLastString.Contains("eta") && count==0) ss >> etalow;
        else if (storeLastString.Contains("eta") && count==1) {ss >> etahigh; storeLastString = ""; }
        else if (storeLastString.Contains("pt") && count==0) ss >> ptlow;
        else if (storeLastString.Contains("pt") && count==1) {ss >> pthigh; storeLastString = ""; }
        else if (storeLastString.Contains("NPV") && count==0) ss >> NPVlow;
        else if (storeLastString.Contains("NPV") && count==1) {ss >> NPVhigh; storeLastString = ""; }
        else {std::cout << "Unexpected result! StoreLastString = " << storeLastString << " and count = " << count << " and this token is " << thistoken << std::endl; return 0;}
        count++;
      }
      if (thistoken.Contains("Eta",TString::kIgnoreCase)) { storeLastString = "eta"; count = 0; }
      else if (thistoken.Contains("pt",TString::kIgnoreCase)) { storeLastString = "pt"; count = 0; }
      else if (thistoken.Contains("NPV",TString::kIgnoreCase)) { storeLastString = "NPV"; count = 0; }
    }

    // Store binning for making plots later
    if (pTBins.size() == 0) pTBins.push_back(15.0);
    if (std::find(pTBins.begin(), pTBins.end(), pthigh) == pTBins.end()) pTBins.push_back(pthigh);
    if (etaBins.size() == 0) etaBins.push_back(0.0);
    if (std::find(etaBins.begin(), etaBins.end(), etahigh) == etaBins.end()) etaBins.push_back(etahigh);
    if (NPVBins.size() == 0) NPVBins.push_back(0.0);
    if (std::find(NPVBins.begin(), NPVBins.end(), NPVhigh) == NPVBins.end()) NPVBins.push_back(NPVhigh);

    double fitMin = minPt/ptlow;
    TH1F *h = (TH1F*)f->Get(inHistName);
    h->SetStats(0);
    myFitter->FitAndDraw(h,fitMin);
    can->Print(pdf);

    // Results vector holds pTLow, pTHigh, etaLow, etaHigh, NPVlow, NPVhigh, mean, meanErr, width, widthErr
    std::vector<double> theseresults;
    theseresults.push_back(ptlow); theseresults.push_back(pthigh);
    theseresults.push_back(etalow); theseresults.push_back(etahigh);
    theseresults.push_back(NPVlow); theseresults.push_back(NPVhigh);
    theseresults.push_back(myFitter->GetMean());
    if (myFitter->GetMean() > 0.0) {
      theseresults.push_back(myFitter->GetMeanError());
      theseresults.push_back(myFitter->GetSigma()/myFitter->GetMean());
      theseresults.push_back(myFitter->GetSigmaError()/myFitter->GetMean());
    } else {
      theseresults.push_back(0); theseresults.push_back(0); theseresults.push_back(0);
    }
    results.push_back(theseresults);

  }
  can->SetLogx();
  
  // Sort pT bins so we can use them to make a histogram.
  // Sort the other ones so they don't drive me nuts.
  std::sort(pTBins.begin(), pTBins.end());
  std::sort(etaBins.begin(), etaBins.end());
  std::sort(NPVBins.begin(), NPVBins.end());

  // Make one histogram per eta and NPV bin and write to file.
  TFile outputfile(outputFileName, "RECREATE");
  outputfile.cd();
  
  for (unsigned int etaBin = 0; etaBin < etaBins.size()-1; etaBin++) {
    for (unsigned int NPVBin = 0; NPVBin < NPVBins.size()-1; NPVBin++) {
      TString meanHistName = Form("avgResponseVsPt_absEta_%.1f_%.1f_NPV_%i_%i",etaBins.at(etaBin),etaBins.at(etaBin+1),int(NPVBins.at(NPVBin)),int(NPVBins.at(NPVBin+1)));
      TString widthHistName = Form("responseWidthVsPt_absEta_%.1f_%.1f_NPV_%i_%i",etaBins.at(etaBin),etaBins.at(etaBin+1),int(NPVBins.at(NPVBin)),int(NPVBins.at(NPVBin+1)));
      TH1D h_mean(meanHistName,meanHistName,pTBins.size()-1,&pTBins[0]);
      TH1D h_width(widthHistName,widthHistName,pTBins.size()-1,&pTBins[0]);
 
      // Fill histograms with corresponding results
      for (unsigned int thisresult=0; thisresult < results.size(); thisresult++) {
        std::vector<double> result = results.at(thisresult);
        double useEta = (result.at(3)+result.at(2))/2.0;
        double useNPV = (result.at(5)+result.at(4))/2.0;
        double usePt = (result.at(1)+result.at(0))/2.0;
        if (useEta > etaBins.at(etaBin) && useEta < etaBins.at(etaBin+1) && useNPV > NPVBins.at(NPVBin) && useNPV < NPVBins.at(NPVBin+1)) {
          h_mean.SetBinContent(h_mean.FindBin(usePt),result.at(6));
          h_mean.SetBinError(h_mean.FindBin(usePt),result.at(7));
          h_width.SetBinContent(h_width.FindBin(usePt),result.at(8));
          h_width.SetBinError(h_width.FindBin(usePt),result.at(9));
        }
      }
      
      // Hist is now full. Save to file.
      h_mean.Write();
      h_width.Write();
      std::cout << "Wrote " << meanHistName << " and " << widthHistName << " to file." << std::endl;
      
      // Also draw these to our pdf
      DrawHisto(&h_mean,"Mean of fit",0.8,1.2);
      myFitter->ResetTextCounters();
      myFitter->DrawTextLeft(jetDesc); myFitter->DrawTextLeft(fitDesc);
      can->Print(pdf);

      DrawHisto(&h_width,"Width/Mean of fit",0.0,0.6);
      myFitter->ResetTextCounters();
      myFitter->DrawTextLeft(jetDesc); myFitter->DrawTextLeft(fitDesc);
      can->Print(pdf);

    }
  }
  outputfile.Close();
  std::cout << "Wrote " << outputFileName << std::endl;

  can->Print(pdf+"]");

  printf("\nProduced:\n  %s\n\n",pdf.Data());
}
