#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include "PlottingTools/PlottingTools.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TFrame.h>

using namespace std;
using namespace PlottingTools;

void DrawHisto(TH1F *h, TString ytit, double min, double max) {
  h->GetYaxis()->SetRangeUser(min,max); h->SetXTitle("#it{p}_{T}^{ref} [GeV]"); h->SetYTitle(ytit);
  h->SetStats(0); h->Draw();
}

int main(int argc, char **argv) {
  SetAtlasStyle();

  char _namebuffer[100];
  vector<double> PtBins;
  
  TString _histname  = "hRecoTrueRatio_btag77";
  TString _filename  = "";
  string _scaleLabel = "";
  string _dofit="false";
  
  int ip=1;
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--histname") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _histname=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--file") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _filename=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--scaleLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _scaleLabel=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno scale name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--doFit") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _dofit=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nNo valid argument given to doFit"<<std::endl; break;}
      }
    }
    /////////////////////////////////////////////////
    // If no options specified assume there's only
    // one argument and that argument is the filename
    /////////////////////////////////////////////////
    else { 
      cout << "Arguments must be specified using --file or --histname" << endl;
      cout << "Since this was not done assuming the first argument given is the filename" << endl;
      cout << "Using 'hResponseProbePT' as the histname" << endl;
      _filename=argv[ip];
      ip=argc;
    }
  }
  
  // ////////////////////////////////////////////////
  // Vectors to store the fitted and arithmatic means
  // of the response distributions for each Pt bin
  // ////////////////////////////////////////////////
  vector<double> _mpfmean;
  vector<double> _mpfmeanError;
  vector<double> _mpfgausfit;
  vector<double> _mpfgausfitError;

 
  char prefix[40];
  if (_histname.Contains("Responsef"))  sprintf( prefix, "Responsef");
  else if (_histname.Contains("Responsee"))  sprintf( prefix, "Responsee");
  else if (_histname.Contains("Response"))  sprintf( prefix, "Response");
  else if (_histname.Contains("Balance")) sprintf( prefix, "Balance");

  // /////////////////////////////////////////////////
  // Canvas which will hold the response distributions
  // for each PT bin
  // /////////////////////////////////////////////////

  //sprintf( _namebuffer, "%s_distributions_in_PRef_bins", prefix);
  sprintf( _namebuffer, "RecoTruthRatio_in_PRef_bins");
  TCanvas* binnedRespVsPtCan = new TCanvas( _namebuffer, _namebuffer );

  // //////////////////////////////////////////////////
  // Canvas and graph which will display response Vs Pt
  // //////////////////////////////////////////////////
  //sprintf( _namebuffer, "%s_vs_PtRef", prefix);
  sprintf( _namebuffer, "RecoTruthRatio_vs_PtRef");
  TCanvas* RespVsPtCan = new TCanvas( _namebuffer, _namebuffer );

  TGraphErrors* RespVsPt = new TGraphErrors();
  //sprintf( _namebuffer, "%sVsProbePT_graph", prefix );
  sprintf( _namebuffer, "RecoTruthRatioVsPTRef_graph");
  RespVsPt->SetName(_namebuffer);
  RespVsPt->SetTitle(_namebuffer);

  // ////////////////////////////////////////////////////
  // Canvas and graph which will display resolution Vs Pt
  // ////////////////////////////////////////////////////
  
  //sprintf( _namebuffer, "%s_Resolution_vs_PtRef", prefix);
  sprintf( _namebuffer, "RecoTruthRatio_Resolution_vs_PtRef");
  TCanvas* ResolutionVsPtCan = new TCanvas( _namebuffer, _namebuffer );

  TGraphErrors* ResolutionVsPt = new TGraphErrors();
  //sprintf( _namebuffer, "%sVsProbePT_Resolution_graph", prefix );
  sprintf( _namebuffer, "RecoTruthRatio_Resolution_graph", prefix );
  ResolutionVsPt->SetName(_namebuffer);
  ResolutionVsPt->SetTitle(_namebuffer);

  gErrorIgnoreLevel=2000; // removes Canvas print statements
  TFile *_infile = TFile::Open(_filename, "UPDATE");

  // ////////////////////////////////////////////////
  // The JES fitting tool which should be used in all 
  // JES analysis.
  // /////////////////////////////////////////////////
//  JES_BalanceFitter *_JESfitter = new JES_BalanceFitter(NsigmaForFit);

  // /////////////////////////////////////////////////
  // Read Pt bins form the histogram itself instead of
  // having to input them, makes transition from 
  // Z+jet to gamma+jet easier. 
  // /////////////////////////////////////////////////
  sprintf( _namebuffer, _histname );
  TH2D* hist2d = (TH2D*)_infile->Get( _namebuffer);

  //hist2d->GetXaxis()->SetRangeUser(800, 1200);
  hist2d->SetAxisRange(800, 1200);
 

 
  for(int i=1;i<=hist2d->GetXaxis()->GetNbins()+1;++i) PtBins.push_back(hist2d->GetXaxis()->GetBinLowEdge(i));

  char variable[40];
  sprintf(variable, "p_{T}^{Ref}");
  ResponseVs( hist2d, binnedRespVsPtCan, RespVsPt, ResolutionVsPt, prefix, variable, false);

  TString NameShort = TString(_filename(0,_filename.Length()-5));
  sprintf( _namebuffer, "RecoTruthRatio_in_PRef_bins.pdf");
  binnedRespVsPtCan->SaveAs(_namebuffer);
  sprintf( _namebuffer, "RecoTruthRatio_in_PRef_bins.root");
  binnedRespVsPtCan->SaveAs(_namebuffer);
  RespVsPtCan->cd();

// Just here temporarily, litterally just ripped out of Jiri's old plotting macro
  TLatex *p = new TLatex(200, 0.6, _scaleLabel.c_str());
  p->SetTextFont(42);
  p->SetTextColor(1);

  RespVsPt->SetMarkerStyle(20);
  RespVsPt->SetMarkerSize(0.7);
  RespVsPt->SetMarkerColor(1);
  RespVsPt->SetLineWidth(1);
  RespVsPt->SetLineColor(1);

  TVirtualPad* pad = RespVsPtCan->cd(1);
  pad->Clear();
  pad->SetPad(0.0, 0.0, 1.0, 1.0);
  pad->GetFrame()->SetBorderMode(0);
  pad->SetBorderSize(5);
  pad->SetTopMargin(0.1);
  pad->SetRightMargin(0.1);
  pad->SetLeftMargin(0.15);
  pad->SetBottomMargin(0.15);

  gPad -> SetLogx();
  float xmin = 10.;
  float xmax = 2000.;
  float ymin = 0.31;
  float ymax = 1.1;

  TString xTitle = "P_{T}^{Ref}";
  TString yTitle = "R_{MPF}";
  if (_histname.Contains("Ratio")) yTitle = "Ratio";
  TH1F* hback = new TH1F( "hback", (char*) 0, 1, xmin, xmax );
  hback->SetMinimum( ymin );
  hback->SetMaximum( ymax );
  // x-axis
  hback->GetXaxis()->SetMoreLogLabels();
  if ( xTitle != "" ) hback->GetXaxis()->SetTitle( xTitle );
  hback->GetXaxis()->SetTitleSize(0.05);
  hback->GetXaxis()->SetTitleOffset(1.2);
  hback->GetXaxis()->SetLabelSize(0.05);
  hback->GetXaxis()->SetTickLength(0.03);
  hback->GetXaxis()->SetLabelOffset(0.01);
  hback->GetXaxis()->SetRangeUser( xmin, xmax );
  hback->GetXaxis()->SetNoExponent();
  // y-axis
  hback->GetYaxis()->SetMoreLogLabels();
  if ( yTitle != "" ) hback->GetYaxis()->SetTitle( yTitle );
  hback->GetYaxis()->SetTitleFont(42);
  hback->GetYaxis()->SetTitleSize(0.05);
  hback->GetYaxis()->SetTitleOffset(1.4);
  hback->GetYaxis()->SetLabelSize(0.05);
  hback->GetYaxis()->SetDecimals();
  hback->SetStats(kFALSE);
  hback->Draw("AXIS");

  if (_dofit == "true") {
    TF1 fit;
    bool usePWR=false;
    bool useLOG2=false;
    bool useLOG3=true;
    bool _useLowETbiasTerm=false;
    double fitMin=20;
    double fitMax=xmax;
    fit = FitResponse( RespVsPt, 
                       fitMin, 
                       fitMax, 
                        usePWR, useLOG2, useLOG3, _useLowETbiasTerm );
    fit.SetRange( fitMin, fitMax);
    RespVsPtCan->cd(1); fit.Draw("sameL");
  }
 // end of stolen

  RespVsPt->SetName("RecoTruthRatio_vs_PtRef");
  RespVsPt->Write();

  binnedRespVsPtCan->Write();
  RespVsPtCan->Write();
  RespVsPt->Draw("sameP");
 
  PlotConstantLine( 1.0 , xmin, xmax, 2, 1 );
  p->Draw();
  sprintf( _namebuffer, "RecoTruthRatio_vs_PtRef.pdf", prefix, string(NameShort).c_str());
  RespVsPtCan->SaveAs(_namebuffer);
  sprintf( _namebuffer, "RecoTruthRatio_vs_PtRef.root", prefix, string(NameShort).c_str());
  RespVsPt->SaveAs(_namebuffer);

  ///////////////////////////////////////////////////////
  //Here I'm working on the resolution graph (Cecilia) //
  ///////////////////////////////////////////////////////

  ResolutionVsPtCan->cd();

  TLatex *p2 = new TLatex(200, 0.6, _scaleLabel.c_str());
  p2->SetTextFont(42);
  p2->SetTextColor(1);

  ResolutionVsPt->SetMarkerStyle(20);
  ResolutionVsPt->SetMarkerSize(0.7);
  ResolutionVsPt->SetMarkerColor(1);
  ResolutionVsPt->SetLineWidth(1);
  ResolutionVsPt->SetLineColor(1);

  TVirtualPad* pad2 = ResolutionVsPtCan->cd(1);
  pad2->Clear();
  pad2->SetPad(0.0, 0.0, 1.0, 1.0);
  pad2->GetFrame()->SetBorderMode(0);
  pad2->SetBorderSize(5);
  pad2->SetTopMargin(0.1);
  pad2->SetRightMargin(0.1);
  pad2->SetLeftMargin(0.15);
  pad2->SetBottomMargin(0.15);

  gPad -> SetLogx();
  xmin = 10.;
  xmax = 2000.;
  ymin = -1.;
  ymax = 1.;

  xTitle = "P_{T}^{Ref}";
  yTitle = "Resolution_{MPF}";
  if (_histname.Contains("Ratio")) yTitle = "Ratio";
  TH1F* hback2 = new TH1F( "hback2", (char*) 0, 1, xmin, xmax );
  hback2->SetMinimum( ymin );
  hback2->SetMaximum( ymax );
  // x-axis
  hback2->GetXaxis()->SetMoreLogLabels();
  if ( xTitle != "" ) hback2->GetXaxis()->SetTitle( xTitle );
  hback2->GetXaxis()->SetTitleSize(0.05);
  hback2->GetXaxis()->SetTitleOffset(1.2);
  hback2->GetXaxis()->SetLabelSize(0.05);
  hback2->GetXaxis()->SetTickLength(0.03);
  hback2->GetXaxis()->SetLabelOffset(0.01);
  hback2->GetXaxis()->SetRangeUser( xmin, xmax );
  hback2->GetXaxis()->SetNoExponent();
  // y-axis
  hback2->GetYaxis()->SetMoreLogLabels();
  if ( yTitle != "" ) hback2->GetYaxis()->SetTitle( yTitle );
  hback2->GetYaxis()->SetTitleFont(42);
  hback2->GetYaxis()->SetTitleSize(0.05);
  hback2->GetYaxis()->SetTitleOffset(1.4);
  hback2->GetYaxis()->SetLabelSize(0.05);
  hback2->GetYaxis()->SetDecimals();
  hback2->SetStats(kFALSE);
  hback2->Draw("AXIS");


  ResolutionVsPt->Write();

  ResolutionVsPt->SetName("Resolution_RecoTruthRatio_vs_PtRef");
//  binnedRespVsPtCan->Write();
//  RespVsPtCan->Write();
  ResolutionVsPt->Draw("sameP");

  //PlotConstantLine( 1.0 , xmin, xmax, 2, 1 );
  p2->Draw();
  sprintf( _namebuffer, "Resolution_RecoTruthRatio_vs_PtRef.pdf");
  ResolutionVsPtCan->SaveAs(_namebuffer);
  sprintf( _namebuffer, "Resolution_RecoTruthRatio_vs_PtRef.root");
  ResolutionVsPt->SaveAs(_namebuffer);

}


