#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include "PlottingTools/PlottingTools.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TFrame.h>

using namespace std;
using namespace PlottingTools;

void DrawHisto(TH1F *h, TString ytit, double min, double max) {
  h->GetYaxis()->SetRangeUser(min,max); h->SetXTitle("#it{p}_{T}^{ref} [GeV]"); h->SetYTitle(ytit);
  h->SetStats(0); h->Draw();
}

int main(int argc, char **argv) {
  SetAtlasStyle();

  char _namebuffer[100];
  vector<double> PtBins;

  TString _histname  = "hMappingProbePT";
  TString _filename  = "";
  string _scaleLabel = "";
  
  int ip=1;
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--histname") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _histname=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--file") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _filename=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--scaleLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _scaleLabel=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno scale name specified"<<std::endl; break;}
      }
    }
    /////////////////////////////////////////////////
    // If no options specified assume there's only
    // one argument and that argument is the filename
    /////////////////////////////////////////////////
    else { 
      cout << "Arguments must be specified using --file or --histname" << endl;
      cout << "Since this was not done assuming the first argument given is the filename" << endl;
      cout << "Using 'hMappingProbePT' as the histname" << endl;
      _filename=argv[ip];
      ip=argc;
    }
  }

  // ////////////////////////////////////////////////
  // How far from the arithmatic mean do we allow the
  // fir to see?
  // ////////////////////////////////////////////////
  double NsigmaForFit = 100.6;
  
  // ////////////////////////////////////////////////
  // Min number of events required to attempt a fit. 
  // Use mean for min_N_eff < n <= min_N_eff_for_fit, 
  // -999 otherwise
  // ////////////////////////////////////////////////
  double _min_N_eff = 10;
  double _min_N_eff_for_fit = 999999.;

  // ////////////////////////////////////////////////
  // Vectors to store the fitted and arithmatic means
  // of the response distributions for each Pt bin
  // ////////////////////////////////////////////////
  vector<double> _mpfmean;
  vector<double> _mpfmeanError;
  vector<double> _mpfgausfit;
  vector<double> _mpfgausfitError;

 
  char prefix[40];
  sprintf( prefix, "Mapping");

  float xmin = 10.;
  float xmax = 2000.;
  float ymin = 10;
  float ymax = 2000;


  // /////////////////////////////////////////////////
  // Canvas which will hold the response distributions
  // for each PT bin
  // /////////////////////////////////////////////////

  sprintf( _namebuffer, "%s_distributions_in_PtRef_bins", prefix);
  TCanvas* binnedRespVsPtCan = new TCanvas( _namebuffer, _namebuffer );

  // //////////////////////////////////////////////////
  // Canvas and graph which will display response Vs Pt
  // //////////////////////////////////////////////////
  sprintf( _namebuffer, "%s_vs_PtRef", prefix);
  TCanvas* RespVsPtCan = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
  RespVsPtCan->Divide( 1, 2, 0.0, 0.01, 0 );
  RespVsPtCan->cd(1);
  gPad -> SetLogx();
  RespVsPtCan->cd(2);
  gPad -> SetLogx();
  //PrepareTwoPadCanvas( RespVsPtCan, "p_{T}^{Ref}", "p_{T}^{JES+GSC}", "p_{T}^{JES+GSC}/p_{T}^{Ref}", xmin, xmax, ymin, ymax, 0.8001, 1.199, 505);
  PrepareTwoPadCanvas( RespVsPtCan, "p_{T}^{Ref}", "p_{T}^{JES+GSC}", "p_{T}^{JES+GSC}/p_{T}^{Ref}", xmin, xmax, ymin, ymax, 0.1001, 0.499, 505);

//  TCanvas* RespVsPtCan = new TCanvas( _namebuffer, _namebuffer );

  TGraphErrors* RespVsPt = new TGraphErrors();
  sprintf( _namebuffer, "%sVsProbePT_graph", prefix );
  RespVsPt->SetName(_namebuffer);
  RespVsPt->SetTitle(_namebuffer);


  gErrorIgnoreLevel=2000; // removes Canvas print statements
  TFile *_infile = TFile::Open(_filename, "UPDATE");

  // ////////////////////////////////////////////////
  // The JES fitting tool which should be used in all 
  // JES analysis.
  // /////////////////////////////////////////////////
//  JES_BalanceFitter *_JESfitter = new JES_BalanceFitter(NsigmaForFit);

  // /////////////////////////////////////////////////
  // Read Pt bins form the histogram itself instead of
  // having to input them, makes transition from 
  // Z+jet to gamma+jet easier. 
  // /////////////////////////////////////////////////
  sprintf( _namebuffer, _histname );
  TH2D* hist2d = (TH2D*)_infile->Get( _namebuffer);
  for(int i=1;i<=hist2d->GetXaxis()->GetNbins()+1;++i) PtBins.push_back(hist2d->GetXaxis()->GetBinLowEdge(i));

  char variable[40];
  sprintf(variable, "p_{T}^{Ref}");
  ResponseVs( hist2d, binnedRespVsPtCan, RespVsPt, prefix, variable, false, _min_N_eff, _min_N_eff_for_fit);
  TString NameShort = TString(_filename(0,_filename.Length()-5));
  sprintf( _namebuffer, "%s_%sVsPt_bins.pdf", string(NameShort).c_str(), prefix);
  binnedRespVsPtCan->SaveAs(_namebuffer);
  sprintf( _namebuffer, "%s_%sVsPt_bins.root", string(NameShort).c_str(), prefix);
  binnedRespVsPtCan->SaveAs(_namebuffer);
  RespVsPtCan->cd(1);


// Just here temporarily, litterally just ripped out of Jiri's old plotting macro
  TLatex *p = new TLatex(200, 0.6, _scaleLabel.c_str());
  p->SetTextFont(42);
  p->SetTextColor(1);

  RespVsPt->SetMarkerStyle(20);
  RespVsPt->SetMarkerSize(1.0);
  RespVsPt->SetMarkerColor(1);
  RespVsPt->SetLineWidth(1);
  RespVsPt->SetLineColor(1);


/*  TVirtualPad* pad = RespVsPtCan->cd(1);
  pad->Clear();
  pad->SetPad(0.0, 0.0, 1.0, 1.0);
  pad->GetFrame()->SetBorderMode(0);
  pad->SetBorderSize(5);
  pad->SetTopMargin(0.1);
  pad->SetRightMargin(0.1);
  pad->SetLeftMargin(0.15);
  pad->SetBottomMargin(0.15);
*/


  gPad -> SetLogx();
  gPad -> SetLogy();

//  float ymin = 0.81;
//  float ymax = 1.19; 
//  TString xTitle = "P_{T}^{Electron, calibrated}";
//  TString yTitle = "P_{T}^{LC topo cluster}/P_{T}^{Electron Cluster}";
  TString xTitle = "P_{T}^{Ref}";
  TString yTitle = "P_{T}^{JES+GSC}";
  TH1F* hback = new TH1F( "hback", (char*) 0, 1, xmin, xmax );
  hback->SetMinimum( ymin );
  hback->SetMaximum( ymax );
  // x-axis
  hback->GetXaxis()->SetMoreLogLabels();
  if ( xTitle != "" ) hback->GetXaxis()->SetTitle( xTitle );
  hback->GetXaxis()->SetTitleSize(0.05);
  hback->GetXaxis()->SetTitleOffset(1.2);
  hback->GetXaxis()->SetLabelSize(0.05);
  hback->GetXaxis()->SetTickLength(0.03);
  hback->GetXaxis()->SetLabelOffset(0.01);
  hback->GetXaxis()->SetRangeUser( xmin, xmax );
  hback->GetXaxis()->SetNoExponent();
  // y-axis
  hback->GetYaxis()->SetMoreLogLabels();
  if ( yTitle != "" ) hback->GetYaxis()->SetTitle( yTitle );
  hback->GetYaxis()->SetTitleFont(42);
  hback->GetYaxis()->SetTitleSize(0.05);
  hback->GetYaxis()->SetTitleOffset(1.4);
  hback->GetYaxis()->SetLabelSize(0.05);
  hback->GetYaxis()->SetDecimals();
  hback->SetStats(kFALSE);
  hback->Draw("AXIS");

  RespVsPt->Write();
  binnedRespVsPtCan->Write();
  RespVsPtCan->Write();
  RespVsPt->Draw("sameP0");

  TF1* f = new TF1( "f", "x", xmin, xmax );
  f->SetLineStyle(2);
  f->SetLineColor(1);
  f->SetLineWidth(1);
  f->Draw("sameL"); 

 
  TGraphErrors* gRatio = new TGraphErrors();
  gRatio->SetMarkerStyle(20);
  gRatio->SetMarkerSize(1.0);
  gRatio->SetMarkerColor(1);

  for ( int n = 0; n < RespVsPt->GetN()-1; ++n ) {
    cout << RespVsPt->GetX()[n] << " " << RespVsPt->GetY()[n] << " " << RespVsPt->GetY()[n] / RespVsPt->GetX()[n] << " " << RespVsPt->GetX()[n] - RespVsPt->GetY()[n] << endl;
    gRatio->SetPoint(n, RespVsPt->GetX()[n] , RespVsPt->GetY()[n] / RespVsPt->GetX()[n]);
    double yerr = RespVsPt->GetY()[n] / RespVsPt->GetX()[n] * RespVsPt->GetEY()[n] / RespVsPt->GetY()[n];
    gRatio->SetPointError(n, RespVsPt->GetEX()[n], yerr); //RespVsPt->GetEY()[n]);
  }
  RespVsPtCan->cd(2);
  PlotConstantLine( 1.0 , xmin, xmax, 2, 1 );
  PlotConstantLine( 0.95, xmin, xmax, 3, 1 );
  PlotConstantLine( 1.05, xmin, xmax, 3, 1 );
  gRatio->Draw("sameP");
 

  p->Draw();
  sprintf( _namebuffer, "%sVsPt_%s.pdf", prefix, string(NameShort).c_str());
  RespVsPtCan->SaveAs(_namebuffer);



}
