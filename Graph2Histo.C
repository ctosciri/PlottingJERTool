#include <vector>

std::string _btag = "_btag77";
std::string _sample = "_Sherpa";

void Graph2Histo()
{

  gROOT->Macro("/afs/cern.ch/user/c/ctosciri/public/atlasstyle-00-03-05/AtlasStyle.C");
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

  TFile *input_file = TFile::Open("Plot_NewBins/Ratio_RecoTrue/Sherpa/Resolution_RecoTruthRatio_vs_PtRef.root", "READ");
  TFile *output_file = TFile::Open("RatioRecoTruth.root", "RECREATE");
  std::cout << "Input File: " << std::endl;
  input_file->ls();
  std::cout << "Output File: " << std::endl;
  output_file->ls();

   TGraphErrors* Resolution = (TGraphErrors*)input_file->Get("Resolution_RecoTruthRatio_vs_PtRef"); 
 // TH1D * resol_vs_pt = (TH1D *)input_file->Get("h5");

  // Understand bins
  Float_t bins[500] = {0};
  Resolution->Draw();
  std::cout << "Bins: ";
  for (size_t p = 0; p < Resolution->GetN(); p++) {
  cout<< "size Resolution  " << Resolution->GetN() << endl;
    double min, temp;
    Resolution->GetPoint(p, min, temp);
    min -= Resolution->GetErrorX(p);
    std::cout << min << ", ";
    bins[p] = min;
    if (p == Resolution->GetN()-1){
      bins[p+1] = min + 2 * Resolution->GetErrorX(p);
      std::cout << bins[p+1];
    }
  }
  std::cout<<"    End bins."<<std::endl; 

  // Make histogram from TGraphError
  TH1D* h1 = new TH1D("h1", ";p_{T}^{Ref} [GeV];#sigma(p_{T}^{reco jet}/p_{T}^{truth jet})", Resolution->GetN(), bins); // DATA
  for (size_t p = 0; p < Resolution->GetN(); p++) {
    double x, y;
    Resolution->GetPoint(p, x, y);
    h1->SetBinContent(p+1, y);
    h1->SetBinError(p+1, Resolution->GetErrorY(p));
    std::cout << "H1 - Point at " << p << ", x = " << x << ", y = " << y << ", error = " << Resolution->GetErrorY(p) << std::endl;
    if (x < 1) h1->SetBinContent(p+1, -999);
  }


  h1->SetStats(0);      // No statistics on plot
  h1->SetMarkerStyle(23);
  h1->GetXaxis()->SetLabelSize(0.04);
  h1->GetXaxis()->SetTickLength(0.03);
  h1->GetXaxis()->SetLabelOffset(0.01);
  h1->GetXaxis()->SetRangeUser(bins[1],1000);

  h1->SetLineColor(kBlue+1);
  h1->SetLineWidth(2);
  h1->GetXaxis()->CenterTitle();
  h1->SetMarkerStyle(kFullCircle);
  h1->SetMarkerColor(kBlue+1);
  h1->SetMinimum(0.01);  // Define Y ..
  h1->SetMaximum(0.2); // .. range

  // Y axis h1 plot settings
  h1->GetYaxis()->CenterTitle();
  h1->GetYaxis()->SetTitleSize(24);
  h1->GetYaxis()->SetTitleFont(43);
  h1->GetYaxis()->SetTitleOffset(1.03);

  h1->Draw();

 // Save Resolution to file
  output_file->cd();
  //h1->SetTitle(TString("RecoTruthRatio" + _btag + _sample));
  h1->SetName(TString("RecoTruthRatio"+ _btag + _sample));
  h1->Write();
  output_file->Close();

}

