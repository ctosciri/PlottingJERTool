
void JetResponse()
{

  gROOT->Macro("/afs/cern.ch/user/c/ctosciri/public/atlasstyle-00-03-05/AtlasStyle.C");
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

  //SetAtlasStyle();

  TFile *f1 = new TFile("Plot_MC/Plot_btag77/BalanceVsPt_user.ctosciri.merg.root");
  TFile *f2 = new TFile("Plot_MC/Plot_Not_btag77/BalanceVsPt_user.ctosciri.merg.root");

  f1->ls();

  TGraphErrors* RespVsPt_btag = (TGraphErrors*)f1->Get("ResponseVsProbePT_graph");
  TGraphErrors* RespVsPt_Not_btag = (TGraphErrors*)f2->Get("ResponseVsProbePT_graph");

  RespVsPt_btag->SetMarkerColor(kRed+1);  
  RespVsPt_btag->SetMarkerStyle(20);
  RespVsPt_btag->SetMarkerSize(1);
//  RespVsPt_btag->SetLineColor(kRed+1);

  RespVsPt_Not_btag->SetMarkerColor(kBlue+1);
  RespVsPt_Not_btag->SetMarkerStyle(20);
  RespVsPt_Not_btag->SetMarkerSize(1);
//  RespVsPt_Not_btag->SetLineColor(kBlue+1);

  RespVsPt_btag->GetYaxis()->SetTitle("Response (p_{t}^{reco}/p_{t}^{truth})");
  //h_All->GetYaxis()->SetTitle("Resolution (#sigma_{R} /R)");  
  
  //set range for response
  RespVsPt_btag->GetYaxis()->SetRangeUser(0.8,1.2); 
  
  //set range for resolution
  //h_All->GetYaxis()->SetRangeUser(-0.25,0.5); 

  TCanvas *c = new TCanvas("c");
  c->SetLogx();
      
  RespVsPt_btag->Draw("AP");
  RespVsPt_Not_btag->Draw("P");

  TLatex* head = new TLatex(.885, .85, "0.0 < #eta < 0.3");
  head->SetTextColor(kBlack);
  head->SetNDC();
  head->SetTextSize(1.5/30.);
  head->SetTextAlign(32);
  head->Draw();

  TLegend* leg_res = new TLegend(.7, .6, .9, .8);
  gStyle->SetLegendTextSize(0.04);
  leg_res->AddEntry(RespVsPt_btag, "btag", "lep");
  leg_res->AddEntry(RespVsPt_Not_btag, "not_btag", "lep");
  leg_res->Draw();

 //TLegend* leg_res = new TLegend(.6, .4, 1., .75);
/*  TLegend* leg_res = new TLegend(.7, .6, .9, .8);
//gStyle->SetFillStyle(0);   
// gStyle->SetLegendBorderSize(0);
    //leg_res->SetLegendFillColor(0);
    //leg_res->SetLegendFont(42);
    gStyle->SetLegendTextSize(0.04);
    //leg_res->SetFillStyle(0);
    //leg_res->SetHeader("p_{t} resolution");
    leg_res->AddEntry(h_All, "All jets", "lf");
    leg_res->AddEntry(h_B, "b jets", "lf");
    leg_res->AddEntry(h_C, "c jets", "lf");    
    leg_res->AddEntry(h_Light, "light jets", "lf");    
    leg_res->AddEntry(h_Glu, "gluon jets", "lf");  
    //leg_res->AddEntry(h_Other, "other jets", "lf");
    leg_res->Draw();

  // Line at 1 (for response) or at 0 (for resolution)
  double xAxisMin = h_All->GetBinCenter(1)-h_All->GetBinWidth(1)/2.;
  double xAxisMax = h_All->GetBinCenter(h_All->GetNbinsX())+h_All->GetBinWidth(h_All->GetNbinsX())/2.;

  cout << xAxisMin << endl;
  cout << xAxisMax << endl;
  cout << h_All->GetNbinsX() << endl;
  cout << h_All->GetBinWidth(h_All->GetNbinsX());

  TLine *line = new TLine (xAxisMin, 1., xAxisMax, 1.);
  line->SetLineColor(kBlack);
  line->SetLineStyle(2);
  line->Draw();
*/

//  c->SaveAs("Response_plots/Response_eta_0.0_0.3_with_gluon.pdf");

}

