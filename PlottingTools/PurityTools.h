#ifndef PurityTools_PurityTools_h
#define PurityTools_PurityTools_h

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TStyle.h>
#include "TLatex.h"


using namespace std;

namespace PurityTools {

  vector<pair<double,double> > GetPurity(float x1, float x2, float y1, float y2, TH2D *h2);

  void DrawPurity();



}

#endif


